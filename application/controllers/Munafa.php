<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Munafa extends CI_Controller {

public function __construct()
 {
        parent::__construct();
       	 $this->load->helper('form');
       	 $this->load->model('Munafaa_Model');
         $this->load->database();
         $this->load->helper('url');
         $this->load->helper('form');
		 $this->load->library('session');
		 $this->load->library('encryption');
		 $this->load->library('email');
 }

	public function index()
	{
		$data['flag'] = 1;
		$data['featured'] = $this->Munafaa_Model->getItServices();
		$data['featuredDigital'] = $this->Munafaa_Model->getDigitalServices();
		$this->load->view('web/header',$data);
		$this->load->view('web/index',$data);
		$this->load->view('web/footer');
	}

	public function productDetail($id)
	{
		$data['flag'] = 0;
		$service_id = urldecode($id);
		$data['service'] = $this->Munafaa_Model->getService($id);
		$data['related'] = $this->Munafaa_Model->getRelatedService($id);
		$this->load->view('web/header',$data);
		$this->load->view('web/product-detail',$data);
		$this->load->view('web/footer');
	}

	public function partner()
	{
		if(isset($_POST['submit'])){
			$name = $this->input->post("name");
			$nature = $this->input->post("nature");
			$owner = $this->input->post("owner");
			$mobile = $this->input->post("mobile");
			$email = $this->input->post("email");
			$gst = $this->input->post("gst");
			$pan = $this->input->post("pan");
			$aadhar = $this->input->post("aadhar");
			$uaadhar = $this->input->post("uaadhar");
			$city = $this->input->post("city");
			$state = $this->input->post("state");
			$address = $this->input->post("address");
			$landmark = $this->input->post("landmark");
			$accountHolder = $this->input->post("accountHolder");
			$branch = $this->input->post("branch");
			$bank_name = $this->input->post("bank_name");
			$ifsc = $this->input->post("ifsc");
			$result = substr($name, 0, 4);
			$partner_code = $result.'MUN'.rand(9999,1000);
			$rand = rand();
			$target = "uploads/partner/logo/".$rand.basename($_FILES['logo']['name']);	
			$logo= $rand.$_FILES['logo']['name'];
				
			if(move_uploaded_file($_FILES['logo']['tmp_name'],$target)){
				$msg="image uploaded";
			}
			else
			{
				 $msg="error";
			}

			$rand = rand();
			$target = "uploads/partner/cancel/".$rand.basename($_FILES['cancel']['name']);	
			$cancel= $rand.$_FILES['cancel']['name'];
				
			if(move_uploaded_file($_FILES['cancel']['tmp_name'],$target)){
				$msg="image uploaded";
			}
			else
			{
				 $msg="error";
			}
			$data['result'] = $this->Munafaa_Model->partner($name,$nature,$owner,$mobile,$email,$gst,$pan,$aadhar,$city,$state,$address,$landmark,$accountHolder,$branch,$bank_name,$ifsc,$partner_code,$logo,$cancel,$uaadhar);
			$data['flag'] = 0;
			$this->load->view("web/header",$data);
			$this->load->view("web/partner",$data);
			$this->load->view("web/footer");
		}else{
			$data['flag'] = 0;
			$this->load->view("web/header",$data);
			$this->load->view("web/partner");
			$this->load->view("web/footer");
		}
	}

	public function login()
	{
		if(isset($_POST['submit'])){
			$mobile = $this->input->post("mobile");
			$query = $this->db->query("select * from db_user where mobile = '$mobile' and status='active'")->result();
			if(count($query) > 0){
				$rand = rand(9999,1000);
				$query = $this->db->query("update db_user set otp = '$rand' where mobile = '$mobile' and status='active'");
				$data['status'] = 'success';
				$data['mobile'] = $mobile;
				$data['flag'] = 0;
				$this->load->view("web/header",$data);
				$this->load->view("web/login-otp-verify",$data);
				$this->load->view("web/footer");
			}else{
				$data['status'] = 'register';
				$data['flag'] = 0;
				$this->load->view("web/header",$data);
				$this->load->view("web/register",$data);
				$this->load->view("web/footer");
			}
		}else{
			$data['flag'] = 0;
			$this->load->view("web/header",$data);
			$this->load->view("web/Login");
			$this->load->view("web/footer");
		}
	}

	public function register()
	{
		if(isset($_POST['submit'])){
			$name = $this->input->post("name");
			$email = $this->input->post("email");
			$mobile = $this->input->post("mobile");
			$city = $this->input->post("city");
			$address = $this->input->post("address");
			$partnerCode = $this->input->post("partnerCode");
			$otp = rand(9999,1000);
			$auth = substr($name, 0, 4);
			$key = $auth.$otp;
			$data['status'] = $this->Munafaa_Model->registerUser($name,$email,$mobile,$city,$address,$partnerCode,$otp,$key);
			$data['flag'] = 0;
			$data['mobile'] = $mobile;
			$this->load->view("web/header",$data);
			if($data['status'] == 'success'){
			$this->load->view("web/register-otp-verify",$data);
			}else{
				$this->load->view("web/register",$data);
			}
			$this->load->view("web/footer");
		}else{
			$data['flag'] = 0;
			$this->load->view("web/header",$data);
			$this->load->view("web/register");
			$this->load->view("web/footer");
		}
	}

	public function OtpVerifyRegister()
	{
		if(isset($_POST['submit'])){
			$mobile = $this->input->post("mobile");
			$otp = $this->input->post("otp");
			$query = $this->db->query("select * from db_user where mobile = '$mobile' and otp = '$otp' and status ='inactive'")->result();
			if(count($query) > 0){
				$qur = $this->db->query("update db_user set status='active' where mobile='$mobile' and status='inactive'");
				$data=array("id"=>$query[0]->user_id,
							"auth_key"=>$query[0]->auth_key
							);
            		$this->session->set_userdata($data);
				redirect("index");
			}else{
				$data['mobile'] = $mobile;
				$data['flag'] = 0;
				$data['status'] = 'unsuccess';
				$this->load->view("web/header",$data);
				$this->load->view("web/register-otp-verify",$data);
				$this->load->view("web/footer");
			}
		}else{
			$data['flag'] = 0;
			$this->load->view("web/header",$data);
			$this->load->view("web/register-otp-verify");
			$this->load->view("web/footer");
		}
	}

	public function OtpVerifyLogin()
	{
		if(isset($_POST['submit'])){
			$mobile = $this->input->post("mobile");
			$otp = $this->input->post("otp");
			$query = $this->db->query("select * from db_user where mobile = '$mobile' and otp = '$otp' and status ='active'")->result();
			if(count($query) > 0){
				$data=array("id"=>$query[0]->user_id,
							"auth_key"=>$query[0]->auth_key
							);
            	$this->session->set_userdata($data);
				redirect("index");
			}else{
				$data['mobile'] = $mobile;
				$data['flag'] = 0;
				$data['status'] = 'unsuccess';
				$this->load->view("web/header",$data);
				$this->load->view("web/login-otp-verify",$data);
				$this->load->view("web/footer");
			}
		}else{
			$data['flag'] = 0;
			$this->load->view("web/header",$data);
			$this->load->view("web/login-otp-verify");
			$this->load->view("web/footer");
		}
	}

	public function about()
	{
		$data['flag'] = 0;
		$this->load->view("web/header",$data);
		$this->load->view("web/about");
		$this->load->view("web/footer");
	}

	public function addToCart()
	{
		$id = $this->input->post("id");
		$user_id = $this->input->post("user_id");
		$check = $this->db->query("select * from db_cart where user_id = '$user_id' and service_id = '$id'")->result();
		if(count($check) == 0){
		$query = $this->db->query("insert into db_cart set user_id  ='$user_id', service_id = '$id',status='active',datetime='".date("Y-m-d h:i:s")."'");
		}
		$qur = $this->db->query("select * from dg_services where service_id = '$id'")->result();
		?>
		<ol id="cart-sidebar" class="mini-products-list">
		<?php $total = 0; 
		foreach($qur as $row) { ?>
		 <li class="item odd">
			<a class="product-image" title="Modular Modern" href="<?php echo site_url("ProductDetail/".$row->service_id); ?>">
				<img alt="" src="<?php echo base_url().'uploads/product/'.$row->image;?>">
			</a>
			<div class="product-details">
				<a class="btn-remove" onclick="return confirm('Are you sure you would like to remove this item from the shopping cart?');" title="Remove This Item" href="#">Remove This Item</a>
				<a class="btn-edit" title="Edit item" href="#">Edit item</a>
				<p class="product-name">
					<a href="index3-detail.html"><?php echo $row->name; ?></a>
				</p>
				<span class="price"><?php echo $row->price.'/-'; ?></span>
			</div>
		</li>
		<?php $total = $total + $row->price; } 
		?>
		</ol>
		<p class="cart-subtotal">
			<span class="label">Total:</span>
			<span class="price"><?php $total; ?></span>
		</p>
		<?php
	}

	public function cart()
	{
		if(!empty($_SESSION['id'])){
			$id = $_SESSION['id'];
			$data['cartItems'] = $this->db->query("select * from db_cart where user_id  ='$id'")->result();
			$data['flag'] = 0;
			$this->load->view("web/header",$data);
			$this->load->view("web/cart",$data);
			$this->load->view("web/footer");
		}else{
			$data['flag'] = 0;
			$this->load->view("web/header",$data);
			$this->load->view("web/cart");
			$this->load->view("web/footer");
		}
	}

	public function logout()
    {
        unset(
            $_SESSION['id'],
            $_SESSION['auth_key']
        );
        $this->session->sess_destroy();
     redirect("index");
	} 
	
	public function del_cart(){
		$id = $this->input->post("user_id");
		$user_id = $_SESSION['id'];

		$query = $this->db->query("delete from db_cart where service_id = '$id' and user_id ='$user_id'");
	}

	public function clearCart()
	{
		$id = $_SESSION['id'];
		$query = $this->db->query("delete from db_cart where user_id = '$id'");
		redirect("Cart");
	}
}
