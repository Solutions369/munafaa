                <!-- BREADCRUMBS -->
                <div id="sns_breadcrumbs" class="wrap">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div id="sns_titlepage"></div>
                                <div id="sns_pathway" class="clearfix">
                                    <div class="pathway-inner">
                                        <span class="icon-pointer"></span>
                                        <ul class="breadcrumbs">
                                            <li class="home">
                                                <a title="Go to Home Page" href="<?php echo site_url("index"); ?>">
                                                    <i class="fa fa-home"></i>
                                                    <span>Home</span>
                                                </a>
                                            </li>
                                            <li class="category3 last">
                                                <span>Become A Partner</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- AND BREADCRUMBS -->

                <!-- CONTENT -->
                <div id="sns_content" class="wrap layout-m">
                    <div class="container">
                        <div class="row">
                            <div id="contact_gmap" class="col-md-12">
                                <div class="row clearfix">
                                    <div class="col-md-4 contact-info">
                                        
                                    </div>
                                    <div class="col-md-8">
                                    <div class="page-title">
                                    <h1>Become Our Partner</h1>
                                </div>
                                        <div id="contactForm">
                                            <div class="row">
                                            <?php echo form_open_multipart("Partner"); ?>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <input name="name" class="form-control required-entry input-text" id="name" placeholder="Name Of Business" required title="Name" value="" type="text" />
                                                    </div>
                                                    <div class="form-group">
                                                        <input name="nature" class="input-text form-control" name="telephone" id="telephone" placeholder="Nature Of Business" required title="Telephone" value="" type="text" />
                                                    </div>
                                                    <div class="form-group">
                                                        <input name="owner" class="form-control required-entry input-text" id="name" placeholder="Name Of Owner" required title="Name" value="" type="text" />
                                                    </div>
                                                    <div class="form-group">
                                                        <input name="mobile" class="form-control required-entry input-text" id="name" placeholder="Mobile No." required title="Name" value="" type="text" />
                                                    </div>
                                                    <div class="form-group">
                                                        <input name="email" class="form-control input-text required-entry validate-email" id="email" placeholder="E-mail" required title="Email" value="" type="email" />
                                                    </div>
                                                    <div class="form-group">
                                                        <input name="gst" class="form-control required-entry input-text" id="name" placeholder="Gst No." title="Name" value="" type="text" />
                                                    </div>
                                                    <div class="form-group">
                                                        <input name="pan" class="form-control required-entry input-text" id="name" placeholder="Pan No." title="Name" value="" type="text" />
                                                    </div>
                                                    <div class="form-group">
                                                        <input name="aadhar" class="form-control required-entry input-text" id="name" placeholder="Aadhar No." title="Name" value="" type="text" />
                                                    </div>
                                                    <div class="form-group">
                                                        <input name="uaadhar" class="form-control required-entry input-text" id="name" placeholder="Udhyog Aadhar" required title="Name" value="" type="text" />
                                                    </div>
                                                    <div class="form-group">
                                                        <input name="city" class="form-control required-entry input-text" id="name" placeholder="City" required title="Name" value="" type="text" />
                                                    </div>
                                                    <div class="form-group">
                                                        <input name="state" class="form-control required-entry input-text" id="name" placeholder="State" required title="Name" value="" type="text" />
                                                    </div>
                                                    <div class="form-group">
                                                        <textarea name="address" class="form-control required-entry input-text" id="name" placeholder="Address" required title="Name" value="" type="text"></textarea>
                                                    </div>
                                                    <div class="form-group">
                                                        <input name="landmark" class="input-text form-control" name="telephone" id="telephone" placeholder="Landmark" title="Telephone" value="" type="text" />
                                                    </div>
                                                    
                                                    <label>Bank Details</label>
                                                    <div class="form-group">
                                                        <input name="accountHolder" class="input-text form-control" name="telephone" id="telephone" placeholder="Name Of Account Holder" title="Telephone" value="" type="text" />
                                                    </div>
                                                    <div class="form-group">
                                                        <input name="branch" class="input-text form-control" name="telephone" id="telephone" placeholder="Branch" title="Telephone" value="" type="text" />
                                                    </div>
                                                    <div class="form-group">
                                                        <input name="bank_name" class="input-text form-control" name="telephone" id="telephone" placeholder="Bank Name" title="Telephone" value="" type="text" />
                                                    </div>
                                                    <div class="form-group">
                                                        <input name="ifsc" class="input-text form-control" name="telephone" id="telephone" placeholder="IFSC code" title="Telephone" value="" type="text" />
                                                    </div>
                                                    <label>Logo</label>
                                                    <div class="form-group">
                                                        <input name="logo" class="input-text form-control" style="width:100%;" name="telephone" id="telephone" placeholder="IFSC code" title="Telephone" value="" type="file" />
                                                    </div>
                                                    <label>Cancelled Cheque</label>
                                                    <div class="form-group">
                                                        <input name="cancel" class="input-text form-control" style="width:100%;" name="telephone" id="telephone" placeholder="IFSC code" title="Telephone" value="" type="file" />
                                                    </div>
                                                    <div class="form-group">
                                                    <button type="submit" style="width:100%;" name="submit" class="btn btn-success">Submit</button>    
                                                    </div>
                                                </div>
                                            <?php echo form_close(); ?> 
                                                <div class="col-sm-6">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- AND CONTENT -->

                
