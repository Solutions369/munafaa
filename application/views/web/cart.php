            <!-- BREADCRUMBS -->
            
            <div id="sns_breadcrumbs" class="wrap">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="sns_titlepage"></div>
                            <div id="sns_pathway" class="clearfix">
                                <div class="pathway-inner">
                                    <span class="icon-pointer "></span>
                                    <ul class="breadcrumbs">
                                        <li class="home">
                                            <a title="Go to Home Page" href="#">
                                                <i class="fa fa-home"></i>
                                                <span>Home</span>
                                            </a>
                                        </li>
                                        <li class="category3 last">
                                            <span>Shopping cart</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- AND BREADCRUMBS -->

            <!-- CONTENT -->
            <div id="sns_content" class="wrap layout-m">
                <div class="container">
                    <div class="row">
                        <div class="shoppingcart">
                            <div class="sptitle col-md-12">
                                <h3>SHOPPING CART</h3>
                                <h4 class="style">PROCEED TO CHECKOUT</h4>
                            </div>
                            <div class="content col-md-12">
                                <ul class="title clearfix">
                                    <li class="text1"><a href="#">SERVICE NAME</a></li>
                                    <li class="text2"><a href="#">UNIT PRICE</a></li>
                                    <li class="text2"><a href="#">Delete</a></li>
                                </ul>
                                <?php $total = 0; foreach($cartItems as $row){ 
                                    $query = $this->db->query("select * from dg_services where service_id = '$row->service_id'")->result();
                                    ?>
                                <ul class="nav-mid clearfix">
                                    <?php if(!empty($query[0]->image)){ ?>
                                    <li class="image"><a href="#"><img src="<?php echo base_url().'uploads/products/'.$query[0]->image; ?>" alt="" style="width:100px"></a></li>
                                    <?php }else{ ?>
                                        <li class="image"><a href="#"><img src="<?php echo base_url().'assets/images/default.png';?>" alt="" style="width:100px;"></a></li>
                                        
                                    <?php }?>
                                    <li class="item-title"><a href="<?php echo site_url("ProductDetail/".$query[0]->service_id); ?>"><?php echo $query[0]->name; ?></a></li>
                                    <li class="price2"><?php echo $query[0]->price; ?></li>
                                    <li class="icon2"><i class="btn-remove fa fa-remove del" id="<?php echo $query[0]->service_id; ?>"></i></li>
                                </ul>
                                <?php $total = $total + $query[0]->price; } ?>
                                 <ul class="nav-bot clearfix">
                                    <li class="continue"><a href="<?php echo site_url("index"); ?>">Continue shopping</a></li>
                                    <li class="clear"><a href="<?php echo site_url("clearCart"); ?>">clear shopping cart</a></li>
                                </ul>
                                <div class="row">
                                    <form class="col-md-4">
                                        <div class="form-bd">
                                            <h3>ESTIMATE SHIPPING AND TAX</h3>
                                            <p class="text1">Enter your destination to get a shipping estimate.</p>
                                            <p class="country">
                                                <span class="color1">*</span>Country
                                            </p>
                                            <select id="country" class="validate-select" title="Country" name="country_id">
                                                <option value="AF">Afghanistan</option>
                                                <option value="AL">Albania</option>
                                                <option value="DZ">Algeria</option>
                                                <option value="AS">American Samoa</option>
                                                <option value="AD">Andorra</option>
                                                <option value="AO">Angola</option>
                                                <option value="AI">Anguilla</option>
                                            </select>

                                            <p>State/Province</p>

                                            <select id="region_id" class="required-entry validate-select" title="State/Province" name="region_id">
                                                <option value="">Please select region, state or province</option>
                                                <option value="1">Alabama</option>
                                                <option value="2">Alaska</option>
                                                <option value="3">American Samoa</option>
                                                <option value="4">Arizona</option>
                                                <option value="5">Arkansas</option>
                                                <option value="6">Armed Forces Africa</option>
                                                <option value="7">Armed Forces Americas</option>
                                                <option value="8">Armed Forces Canada</option>
                                                <option value="9">Armed Forces Europe</option>
                                                <option value="10">Armed Forces Middle East</option>
                                            </select>
                                            <p class="zip">Zip/Postal Code</p>
                                            <input class="style23" type="text" value="" size="30" />
                                            <span class="style-bd">Get a quote</span>
                                        </div>
                                    </form>
                                    <form class="col-md-4">
                                        <div class="form-bd">
                                            <h3>DISCOUNT CODES</h3>
                                            <p class="formbd2">Enter your coupon code if you have one.</p>
                                            <input class="styleip" type="text" value="" size="30" />
                                            <span class="style-bd">Apply coupon</span>
                                        </div>
                                    </form>
                                    <form class="form-right col-md-4">
                                        <div class="form-bd">
                                            <p class="subtotal">
                                                <span class="text1">SUBTOTAL:</span>
                                                <span class="text2"><?php echo $total.'/-'; ?></span>
                                            </p>
                                            <h3>
                                                <span class="text3">GRAND TOTAL:</span>
                                                <span class="text4"><?php echo $total.'/-'; ?></span>
                                            </h3>
                                            <span class="style-bd">Proceed to checkout</span>
                                            <p class="checkout">Checkout with Multiple Addresses</p>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- AND CONTENT -->
