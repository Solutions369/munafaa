<!DOCTYPE html>
<html lang="en-US">
        <title>Munafa Mall</title>
        <link href='https://fonts.googleapis.com/css?family=Poppins:300,700' rel='stylesheet' type='text/css'>
        <!-- Style Sheet-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/font/font-awesome/css/font-awesome.min.css';?>" />
        <link rel="stylesheet" href="<?php echo base_url().'assets/css/bootstrap.css';?>">
        <link rel="stylesheet" href="<?php echo base_url().'assets/css/style.css';?>">
        <link rel="stylesheet" href="<?php echo base_url().'assets/js/owl-carousel/owl.carousel.css';?>">
        <link rel="stylesheet" href="<?php echo base_url().'assets/js/owl-carousel/owl.theme.css';?>">
        <link href="<?php echo base_url().'assets/toast/build/toastr.css';?>" rel="stylesheet">
        
        <!-- favicon -->
        <link rel="shortcut icon" href="images/favicon.ico">
        <!-- META TAGS -->
        <meta name="viewport" content="width=device-width" />
    </head>
    <body id="bd" class=" cms-index-index3 header-style3 cms-simen-home-page-v2 default cmspage">
        <div id="sns_wrapper">      
            <!-- HEADER -->
            <div id="sns_header" class="wrap">
                <div id="sns_header_full">
                    <!-- Header Top -->
                    <div class="sns_header_top">
                        <div class="container">
                            <div class="sns_module" style="clear: both">
                                <div class="header-setting">
                                    <div class="module-setting">
                                        <!-- <div class="mysetting language-switcher">
                                            <div class="tongle">
                                                <img alt="" src="<?php echo base_url().'assets/images/flag/english.png';?>">
                                                <span>English</span>
                                            </div>
                                            <div class="content">
                                                <div class="language-switcher">
                                                    <ul id="select-language">
                                                        <li>
                                                            <a  href="index.html">
                                                            <img alt="" src="<?php echo base_url().'assets/images/flag/english.png';?>">
                                                            <span>English</span>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="index2.html">
                                                            <img alt="" src="<?php echo base_url().'assets/images/flag/brazil.png';?>">
                                                            <span>Brazil</span>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a class="selected" href="index3.html">
                                                            <img alt="" src="<?php echo base_url().'assets/images/flag/france.png';?>">
                                                            <span>France</span>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="index4.html">
                                                            <img alt="" src="<?php echo base_url().'assets/images/flag/russian.png';?>">
                                                            <span>Russian</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>   -->

                                        <div class="mysetting currency-switcher">
                                            <!-- <div class="tongle">
                                                <span class="gfont"> USD </span>
                                            </div>
                                            <div class="content">
                                                <ul id="select-currency">
                                                    <li>
                                                        <a href="#"> EUR </a>
                                                    </li>
                                                    <li>
                                                        <a class="selected" href="#"> USD </a>
                                                    </li>
                                                </ul>
                                            </div> -->
                                        </div>
                                    </div>
                                </div>
                                <div class="header-account">
                                    <div class="myaccount">
                                        <div class="tongle">
                                            <i class="fa fa-user"></i>
                                            <span>My account</span>
                                            <i class="fa fa-angle-down"></i>
                                        </div>
                                        <div class="customer-ct content">
                                            <ul class="links">
                                                <li class="first">
                                                    <a class="top-link-myaccount" title="My Account" href="<?php echo site_url("Partner"); ?>">Become A Partner</a>
                                                </li>
                                                <!-- <li>
                                                    <a class="top-link-wishlist" title="My Wishlist" href="#">My Wishlist</a>
                                                </li>
                                                <li>
                                                    <a class="top-link-checkout" title="Checkout" href="#">Checkout</a>
                                                </li> -->
                                                <li class=" last">
                                                    <?php if(empty($_SESSION['id'])){ ?>
                                                    <a class="top-link-login" title="Log In" href="<?php echo site_url("Login"); ?>">Login</a>
                                                    <?php }else{ ?>
                                                    <a class="top-link-login" title="Logout" href="<?php echo site_url("Logout"); ?>">Logout</a>
                                                    <?php } ?>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Menu -->
                    <div id="sns_menu">
                        <div class="container">
                            <div class="row">
                                <div id="sns_header_logo" class="col-md-2 col-sm-3 col-xs-12">
                                    <h1 id="logo" class="responsv">
                                        <a href="<?php echo site_url("index"); ?>" title="Munafa Mall">
                                         <img alt="" src="<?php echo base_url().'assets/images/munafa.png';?>"> 
                                        </a>
                                    </h1>
                                   
                                </div>
                                <div class="sns_mainmenu col-md-9 col-sm-8 col-xs-12">
                                    <div id="sns_mainnav">
                                        <div id="sns_custommenu" class="visible-md visible-lg">
                                            <ul class="mainnav">
                                                <li class="level0 custom-item active">
                                                    <a class="menu-title-lv0 pd-menu116" href="<?php echo site_url("index"); ?>" target="_self">
                                                        <span class="title">Home</span>
                                                    </a>
                                                </li>
                                                <li class="level0 nav-1 no-group first drop-submenu parent">
                                                    <a class=" menu-title-lv0" href="<?php echo site_url("AboutUs"); ?>">
                                                        <span class="title">About Us</span>
                                                    </a>
                                                </li>
                                                <li class="level0 nav-3 no-group drop-submenu12 custom-itemdrop-staticblock">
                                                    <a class=" menu-title-lv0" href="#">
                                                        <span class="title">Offered Services</span>
                                                    </a>
                                                    <div class="wrap_dropdown fullwidth">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                <h6 class="title menu1-2-5">IT SERVICES</h6>
                                                                <ul class="level1">
                                                                <?php $itMenu = $this->db->query("select * from dg_services where category_id = '1'")->result();
                                                                foreach($itMenu as $menu){ ?>
                                                                    <li class="level2 nav-1-3-16 first">
                                                                        <a class=" menu-title-lv2" href="<?php echo site_url("ProductDetail/".$menu->service_id); ?>">
                                                                            <span class="title"><?php echo $menu->name; ?></span>
                                                                        </a>
                                                                    </li>
                                                                <?php } ?>
                                                                </ul>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <h6 class="title menu1-2-5">DIGITAL SERVICES</h6>
                                                                <ul class="level1">
                                                                <?php $digiMenu = $this->db->query("select * from dg_services where category_id = '2'")->result();
                                                                foreach($digiMenu as $menu1){ ?>
                                                                    <li class="level2 nav-1-3-16 first">
                                                                        <a class=" menu-title-lv2" href="<?php echo site_url("ProductDetail/".$menu1->service_id); ?>">
                                                                            <span class="title"><?php echo $menu1->name; ?></span>
                                                                        </a>
                                                                    </li>
                                                                <?php } ?>
                                                                   
                                                                </ul>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <h6 class="title menu1-2-5">OTHER SERVICES</h6>
                                                                <ul class="level1">
                                                                <?php $OMenu = $this->db->query("select * from dg_services where category_id = '3'")->result();
                                                                foreach($OMenu as $menu2){ ?>
                                                                    <li class="level2 nav-1-3-16 first">
                                                                        <a class=" menu-title-lv2" href="<?php echo site_url("ProductDetail/".$menu2->service_id); ?>">
                                                                            <span class="title"><?php echo $menu2->name; ?></span>
                                                                        </a>
                                                                    </li>
                                                                <?php } ?>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                
                                                <li class="level0 custom-item">
                                                    <a class="menu-title-lv0" href="index3-blog.html">
                                                        <span class="title">Blog</span>
                                                    </a>
                                                </li>
                                                <li class="level0 custom-item">
                                                    <a class="menu-title-lv0" href="#">
                                                        <span class="title">Contact Us</span>
                                                    </a>
                                                </li>
                                                <li class="level0 custom-item">
                                                    <a class="menu-title-lv0" href="<?php echo site_url("Cart"); ?>">
                                                        <span class="title">Cart</span>
                                                    </a>
                                                </li>
                                                </ul>
                                        </div>
                                        <div id="sns_mommenu" class="menu-offcanvas hidden-md hidden-lg">
                                            <span class="btn2 btn-navbar leftsidebar" style="display: inline-block;">
                                                <i class="fa fa-align-left"></i>
                                                <span class="overlay"></span>
                                            </span>
                                            <span class="btn2 btn-navbar offcanvas">
                                                <i class="fa fa-align-justify"></i>
                                                <span class="overlay"></span>
                                            </span>
                                            <span class="btn2 btn-navbar rightsidebar">
                                                <i class="fa fa-align-right"></i>
                                                <span class="overlay"></span>
                                            </span>
                                            <div id="menu_offcanvas" class="offcanvas">
                                                <ul class="mainnav">
                                                    <li class="level0 custom-item">
                                                        <div class="accr_header">
                                                            <a class="menu-title-lv0" href="index3.html">
                                                                <span class="title">Home</span>
                                                            </a>
                                                        </div>
                                                    </li>

                                                    <li class="level0 nav-5 first active">
                                                        <div class="accr_header">
                                                            <a class=" menu-title-lv0" href="index3-listing-grid.html">
                                                                <span>Funiture</span>
                                                            </a>
                                                        </div>
                                                    </li>

                                                    <li class="level0 nav-6 parent">
                                                        <div class="accr_header">
                                                            <a class=" menu-title-lv0" href="#">
                                                                <span>All products</span>
                                                            </a>
                                                            <span class="btn_accor">
                                                                
                                                            </span>
                                                        </div>
                                                        <div class="accr_content" style="display: none;">
                                                            <ul class="level0">
                                                                <li class="level1 nav-5-1 first parent">
                                                                    <div class="accr_header">
                                                                        <a class=" menu-title-lv1" href="#">
                                                                            <span>Bags</span>
                                                                        </a>
                                                                        <span class="btn_accor">
                                                                            
                                                                        </span>
                                                                    </div>
                                                                    <div class="accr_content" style="display: none;">
                                                                        <ul class="level1">
                                                                            <li class="level2 nav-5-1 first">
                                                                                <div class="accr_header">
                                                                                    <a class=" menu-title-lv2" href="#">
                                                                                        <span>Laptop</span>
                                                                                    </a>
                                                                                </div>
                                                                            </li>
                                                                            <li class="level2 nav-5-2 parent">
                                                                                <div class="accr_header">
                                                                                    <a class=" menu-title-lv2" href="#">
                                                                                        <span>Dresses</span>
                                                                                    </a>
                                                                                    <span class="btn_accor">
                                                                                        
                                                                                    </span>
                                                                                </div>
                                                                                <div class="accr_content" style="display: none;">
                                                                                    <ul class="level2">
                                                                                        <li class="level3 nav-5-1-1 first">
                                                                                            <div class="accr_header">
                                                                                                <a class=" menu-title-lv3" href="#">
                                                                                                    <span>Briefs</span>
                                                                                                </a>
                                                                                            </div>
                                                                                        </li>
                                                                                        <li class="level3 nav-5-1-2 last parent">
                                                                                            <div class="accr_header">
                                                                                                <a class=" menu-title-lv3" href="#">
                                                                                                    <span>Blog</span>
                                                                                                </a>
                                                                                                <span class="btn_accor">
                                                                                                    
                                                                                                </span>
                                                                                            </div>
                                                                                            <div class="accr_content" style="display: none;">
                                                                                                <ul class="level3">
                                                                                                    <li class="level4 nav-5-1-1-1 first last">
                                                                                                        <div class="accr_header">
                                                                                                            <a class=" menu-title-lv4" href="#">
                                                                                                                <span>Bands</span>
                                                                                                            </a>
                                                                                                        </div>
                                                                                                    </li>
                                                                                                </ul>
                                                                                            </div>
                                                                                        </li>
                                                                                    </ul>
                                                                                </div>
                                                                            </li>
                                                                            <li class="level2 nav-5-3">
                                                                                <div class="accr_header">
                                                                                    <a class=" menu-title-lv2" href="#">
                                                                                        <span>Cosmetic</span>
                                                                                    </a>
                                                                                </div>
                                                                            </li>
                                                                            <li class="level2 nav-5-4">
                                                                                <div class="accr_header">
                                                                                    <a class=" menu-title-lv2" href="#">
                                                                                        <span>Duffle</span>
                                                                                    </a>
                                                                                </div>
                                                                            </li>
                                                                            <li class="level2 nav-5-5 last">
                                                                                <div class="accr_header">
                                                                                    <a class=" menu-title-lv2" href="#">
                                                                                        <span>Nightwear</span>
                                                                                    </a>
                                                                                </div>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </li>
                                                                <li class="level1 nav-5-2 parent">
                                                                    <div class="accr_header">
                                                                        <a class=" menu-title-lv1" href="#">
                                                                            <span>Shirts</span>
                                                                        </a>
                                                                        <span class="btn_accor">
                                                                            
                                                                        </span>
                                                                    </div>
                                                                    <div class="accr_content" style="display: none;">
                                                                        <ul class="level1">
                                                                            <li class="level2 nav-5-1-6 first">
                                                                                <div class="accr_header">
                                                                                    <a class=" menu-title-lv2" href="#">
                                                                                        <span>Tops</span>
                                                                                    </a>
                                                                                </div>
                                                                            </li>
                                                                            <li class="level2 nav-5-1-7">
                                                                                <div class="accr_header">
                                                                                    <a class=" menu-title-lv2" href="#">
                                                                                        <span>Camis</span>
                                                                                    </a>
                                                                                </div>
                                                                            </li>
                                                                            <li class="level2 nav-5-1-8">
                                                                                <div class="accr_header">
                                                                                    <a class=" menu-title-lv2" href="#">
                                                                                        <span>Helmet</span>
                                                                                    </a>
                                                                                </div>
                                                                            </li>
                                                                            <li class="level2 nav-5-1-9">
                                                                                <div class="accr_header">
                                                                                    <a class=" menu-title-lv2" href="#">
                                                                                        <span>Lingerie</span>
                                                                                    </a>
                                                                                </div>
                                                                            </li>
                                                                            <li class="level2 nav-5-1-10 last">
                                                                                <div class="accr_header">
                                                                                    <a class=" menu-title-lv2" href="#">
                                                                                        <span>Hair</span>
                                                                                    </a>
                                                                                </div>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </li>
                                                                <li class="level1 nav-5-3 parent">
                                                                    <div class="accr_header">
                                                                        <a class=" menu-title-lv1" href="#">
                                                                            <span>Shoes</span>
                                                                        </a>
                                                                        <span class="btn_accor">
                                                                            
                                                                        </span>
                                                                    </div>
                                                                    <div class="accr_content" style="display: none;">
                                                                        <ul class="level1">
                                                                            <li class="level2 nav-5-2-11 first">
                                                                                <div class="accr_header">
                                                                                    <a class=" menu-title-lv2" href="#">
                                                                                        <span>Leathers</span>
                                                                                    </a>
                                                                                </div>
                                                                            </li>
                                                                            <li class="level2 nav-5-2-12">
                                                                                <div class="accr_header">
                                                                                    <a class=" menu-title-lv2" href="#">
                                                                                        <span>Rings</span>
                                                                                    </a>
                                                                                </div>
                                                                            </li>
                                                                            <li class="level2 nav-5-2-13">
                                                                                <div class="accr_header">
                                                                                    <a class=" menu-title-lv2" href="#">
                                                                                        <span>Cocktail</span>
                                                                                    </a>
                                                                                </div>
                                                                            </li>
                                                                            <li class="level2 nav-5-2-14">
                                                                                <div class="accr_header">
                                                                                    <a class=" menu-title-lv2" href="#">
                                                                                        <span>Gloves</span>
                                                                                    </a>
                                                                                </div>
                                                                            </li>
                                                                            <li class="level2 nav-5-2-15 last">
                                                                                <div class="accr_header">
                                                                                    <a class=" menu-title-lv2" href="#">
                                                                                        <span>Clothing</span>
                                                                                    </a>
                                                                                </div>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </li>
                                                                <li class="level1 nav-5-4 last parent">
                                                                    <div class="accr_header">
                                                                        <a class=" menu-title-lv1" href="#">
                                                                            <span>Shapewear</span>
                                                                        </a>
                                                                        <span class="btn_accor">
                                                                            
                                                                        </span>
                                                                    </div>
                                                                    <div class="accr_content" style="display: none;">
                                                                        <ul class="level1">
                                                                            <li class="level2 nav-5-3-16 first">
                                                                                <div class="accr_header">
                                                                                    <a class=" menu-title-lv2" href="#">
                                                                                        <span>Hats</span>
                                                                                    </a>
                                                                                </div>
                                                                            </li>
                                                                            <li class="level2 nav-5-3-17">
                                                                                <div class="accr_header">
                                                                                    <a class=" menu-title-lv2" href="#">
                                                                                        <span>Outerwear</span>
                                                                                    </a>
                                                                                </div>
                                                                            </li>
                                                                            <li class="level2 nav-5-3-18">
                                                                                <div class="accr_header">
                                                                                    <a class=" menu-title-lv2" href="#">
                                                                                        <span>Novelty</span>
                                                                                    </a>
                                                                                </div>
                                                                            </li>
                                                                            <li class="level2 nav-5-3-19">
                                                                                <div class="accr_header">
                                                                                    <a class=" menu-title-lv2" href="#">
                                                                                        <span>Footwear</span>
                                                                                    </a>
                                                                                </div>
                                                                            </li>
                                                                            <li class="level2 nav-5-3-20 last">
                                                                                <div class="accr_header">
                                                                                    <a class=" menu-title-lv2" href="#">
                                                                                        <span>Sundresses</span>
                                                                                    </a>
                                                                                </div>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li class="level0 nav-7">
                                                        <div class="accr_header">
                                                            <a class=" menu-title-lv0" href="#">
                                                                <span>Shop</span>
                                                            </a>
                                                        </div>
                                                    </li>


                                                    <li class="level0 nav-8 last parent">
                                                        <div class="accr_header">
                                                            <a class=" menu-title-lv0" href="#">
                                                                <span>Mobile </span>
                                                            </a>
                                                            <span class="btn_accor">
                                                                
                                                            </span>
                                                        </div>
                                                        <div class="accr_content" style="display: none;">
                                                            <ul class="level0">
                                                                <li class="level1 nav-7-1 first">
                                                                    <div class="accr_header">
                                                                        <a class=" menu-title-lv1" href="#">
                                                                            <span>Cfg products</span>
                                                                        </a>
                                                                    </div>
                                                                </li>
                                                                <li class="level1 nav-7-2">
                                                                    <div class="accr_header">
                                                                        <a class=" menu-title-lv1" href="#">
                                                                            <span>Product types</span>
                                                                        </a>
                                                                    </div>
                                                                </li>
                                                                <li class="level1 nav-7-3 parent">
                                                                    <div class="accr_header">
                                                                        <a class=" menu-title-lv1" href="#">
                                                                            <span>Bicycle</span>
                                                                        </a>
                                                                        <span class="btn_accor">
                                                                            
                                                                        </span>
                                                                    </div>
                                                                    <div class="accr_content" style="display: none;">
                                                                        <ul class="level1">
                                                                            <li class="level2 nav-7-2-1 first">
                                                                                <div class="accr_header">
                                                                                    <a class=" menu-title-lv2" href="#">
                                                                                        <span>Lifestyle</span>
                                                                                    </a>
                                                                                </div>
                                                                            </li>
                                                                            <li class="level2 nav-7-2-2">
                                                                                <div class="accr_header">
                                                                                    <a class=" menu-title-lv2" href="#">
                                                                                        <span>Jackets</span>
                                                                                    </a>
                                                                                </div>
                                                                            </li>
                                                                            <li class="level2 nav-7-2-3 last">
                                                                                <div class="accr_header">
                                                                                    <a class=" menu-title-lv2" href="#">
                                                                                        <span>Scarves</span>
                                                                                    </a>
                                                                                </div>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </li>
                                                                <li class="level1 nav-7-4">
                                                                    <div class="accr_header">
                                                                        <a class=" menu-title-lv1" href="#">
                                                                            <span>Cosmetics</span>
                                                                        </a>
                                                                    </div>
                                                                </li>
                                                                <li class="level1 nav-7-5 last">
                                                                    <div class="accr_header">
                                                                        <a class=" menu-title-lv1" href="#">
                                                                            <span>Bras</span>
                                                                        </a>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </li>

                                                    <li class="level0 custom-item">
                                                        <div class="accr_header">
                                                            <a class="menu-title-lv0" href="index3-404.html">
                                                                <span class="title">Offer</span>
                                                            </a>
                                                        </div>
                                                    </li>
                                                    <li class="level0 custom-item">
                                                        <div class="accr_header">
                                                            <a class="menu-title-lv0" href="#">
                                                                <span class="title">Deal</span>
                                                            </a>
                                                        </div>
                                                    </li>
                                                    <li class="level0 custom-item">
                                                        <div class="accr_header">
                                                            <a class="menu-title-lv0" href="index3-blog.html">
                                                                <span class="title">Blog</span>
                                                            </a>
                                                        </div>
                                                    </li>
                                                    <li class="level0 custom-item">
                                                        <div class="accr_header">
                                                            <a class="menu-title-lv0" href="index3-contact-us.html">
                                                                <span class="title">Contact Us</span>
                                                            </a>
                                                        </div>
                                                    </li>
                                                    
                                                    <li class="level0 custom-item">
                                                        <div class="accr_header">
                                                            <a class="menu-title-lv0" href="#">
                                                                <span class="title">Custom menu</span>
                                                            </a>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="sns_menu_right">
                                        <div class="block_topsearch">
                                             <div class="top-cart">
                                                <!-- <div class="mycart mini-cart">
                                                    <div class="block-minicart">
                                                        <div class="tongle">
                                                            <i class="fa fa-shopping-cart"></i>
                                                            <div class="summary">
                                                                <span class="amount">
                                                                    <a href="#">
                                                                        <span>3</span>
                                                                    </a>
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="block-content content">
                                                            <div class="block-inner">
                                                                <ol id="cart-sidebar" class="mini-products-list">
                                                                    <li class="item odd">
                                                                        <a class="product-image" title="Modular Modern" href="index3-detail.html">
                                                                            <img alt="" src="<?php echo base_url().'assets/images/products/1.jpg';?>">
                                                                        </a>
                                                                        <div class="product-details">
                                                                            <a class="btn-remove" onclick="return confirm('Are you sure you would like to remove this item from the shopping cart?');" title="Remove This Item" href="#">Remove This Item</a>
                                                                            <a class="btn-edit" title="Edit item" href="#">Edit item</a>
                                                                            <p class="product-name">
                                                                                <a href="index3-detail.html">Modular Modern</a>
                                                                            </p>
                                                                            <span class="price">$ 540.00</span>
                                                                        </div>
                                                                    </li>
                                                                </ol>
                                                                <p class="cart-subtotal">
                                                                    <span class="label">Total:</span>
                                                                    <span class="price">$ 540.00</span>
                                                                </p>

                                                                <div class="actions">
                                                                    <a class="button">
                                                                        <span>
                                                                            <span>Check out</span>
                                                                        </span>
                                                                    </a>
                                                                    <a class="button gfont go-to-cart" href="index3-shoppingcart.html">Go to cart</a>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> -->
                                            </div>
                                            <!-- <span class="icon-search"></span>
                                            <div class="top-search">
                                                <div id="sns_serachbox_pro11739847651442478087" class="sns-serachbox-pro">
                                                    <div class="sns-searbox-content">
                                                        <form id="search_mini_form3703138361442478087" method="get" action="">
                                                            <div class="form-search">
                                                                <input id="search3703138361442478087" class="input-text" type="text" value="" name="q" placeholder="Search here...." size="30" autocomplete="off">
                                                                <button class="button form-button" title="Search" type="submit">Search</button>
                                                                <div id="search_autocomplete3703138361442478087" class="search-autocomplete" style="display: none;"></div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
<?php
if($flag != 1){
    ?>
    <style>
        .header-style3 #sns_header #sns_header_full {
            position: relative !important;
            background : #fff;
        }
    </style>
    <?php
}
?>
<?php if(isset($_SESSION['id'])){ ?>
<input type="hidden" class="user_id" value="<?php echo $_SESSION['id']; ?>" />
<?php } ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
$(document).ready(function(){    
 $('.addToCart').click(function(){
    var id = $(this).val();
    var user_id = $(".user_id").val();
    if(id!=''){
      $.ajax({
        url: "<?php echo site_url('Munafa/addToCart')?>",
        data: "id=" + id + "&user_id=" + user_id,
        method: "POST",
        success: function(msg){
            toastr.success('Item Added To Cart');
          $('.block-inner').html(msg);
        }
      });
    }
 });
 $(document).on('click','.del',function(){
               user_id = $(this).attr('id');
	if(confirm("Are you sure you want to delete this?"))
	{
           $.ajax({
			   url :"<?php echo site_url(); ?>/Munafa/del_cart",
			   method : 'POST',
			   data:{user_id:user_id},
			   success:function(data){
				//    alert(data);
                   location.reload();
               }
           });
       }else{
		 return false;
		}
	});
});

</script>
