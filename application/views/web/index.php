                    <!-- Slideshow -->
                <div id="sns_slideshows3">
                    <div id="slishow_wrap12" class="sns-slideshow owl-carousel owl-theme owl-loaded">
                        <div class="item">
                            <img src="<?php echo base_url().'assets/images/sildeshow/3.png';?>" alt="">
                        </div>
                        <div class="item">
                            <img src="<?php echo base_url().'assets/images/sildeshow/page3-2.jpg';?>" alt="">
                        </div>
                        <div class="item">
                            <img src="<?php echo base_url().'assets/images/sildeshow/page3-1.jpg';?>" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <!-- AND HEADER -->

            <!-- CONTENT -->
            <div id="sns_content" class="wrap layout-m">
                <div class="container">
                   <div class="row">
                        <div id="sns_main" class="col-md-12 col-main">
                            <div id="sns_mainmidle">
                                <div class="policy-page3">
                                    <ul class="ca-menu">
                                        <li class="col-md-3 col-sm-6">
                                            <a href="#">
                                                <span class="ca-icon"><i class="fa fa-truck"></i></span>
                                                <div class="ca-content">
                                                    <h2 class="ca-main">Freshipping</h2>
                                                    <h3 class="ca-sub">Lorem Ipsum is simply dummy</h3>
                                                </div>
                                            </a>
                                        </li>
                                        <li class="col-md-3 col-sm-6 rsbd-no">
                                            <a href="#">
                                                <span class="ca-icon" id="heart"><i class="fa fa-dollar"></i></span>
                                                <div class="ca-content">
                                                    <h2 class="ca-main">money back</h2>
                                                    <h3 class="ca-sub">Lorem Ipsum is simply dummy</h3>
                                                </div>
                                            </a>
                                        </li>
                                        <li class="col-md-3 col-sm-6">
                                            <a href="#">
                                                <span class="ca-icon"><i class="fa fa-lock"></i></span>
                                                <div class="ca-content">
                                                    <h2 class="ca-main">perfect security</h2>
                                                    <h3 class="ca-sub">Lorem Ipsum is simply dummy</h3>
                                                </div>
                                            </a>
                                        </li>
                                        <li class="col-md-3 col-sm-6">
                                            <a href="#">
                                                <span class="ca-icon"><i class="fa fa-support"></i></span>
                                                <div class="ca-content">
                                                    <h2 class="ca-main">best support</h2>
                                                    <h3 class="ca-sub">Lorem Ipsum is simply dummy</h3>
                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                </div>


                                <div id="sns_producttaps1" class="sns_producttaps_wraps">
                                    <h3 class="precar">PRODUCT TAPS</h3>
                                  <!-- Nav tabs -->
                                  <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">IT Services</a></li>
                                    <!-- <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Wood chair</a></li>
                                    <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Coffee Tables</a></li>
                                    <li role="presentation"><a href="#bedroom" aria-controls="bedroom" role="tab" data-toggle="tab">Bedroom Furniture</a></li> -->
                                  </ul>

                                  <!-- Tab panes -->
                                  <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="home">
                                        <div class="products-grid row style_grid">
                                        <?php foreach($featured as $row){ ?>
                                            <div class="item col-md-3 col-sm-4 col-xs-6 col-phone-12">
                                                 <div class="item-inner">
                                                     <div class="prd">
                                                         <div class="item-img clearfix">
                                                            <!-- <div class="ico-label">
                                                                <span class="ico-product ico-new">New</span>
                                                                <span class="ico-product ico-sale">Sale</span>
                                                            </div> -->

                                                             <a class="product-image have-additional"
                                                                title="<?php echo $row->name; ?>"
                                                                href="<?php $id = urlencode($row->service_id); echo site_url("ProductDetail/$id"); ?>">
                                                                <span class="img-main">
                                                            <?php if(!empty($row->image)){ ?>
                                                               <img src="<?php echo base_url().'uploads/products/'.$row->image;?>" alt="">
                                                            <?php }else{ ?>
                                                                <img src="<?php echo base_url().'assets/images/default.png'?>" alt="">
                                                            <?php } ?>
                                                                </span>
                                                             </a>
                                                         </div>
                                                         <div class="item-info">
                                                             <div class="info-inner">
                                                                 <div class="item-title">
                                                                     <a title="Modular Modern"
                                                                        href="<?php echo site_url("ProductDetail/$row->service_id"); ?>">
                                                                         <?php echo $row->name; ?> </a>
                                                                 </div>
                                                                 <div class="item-price">
                                                                     <div class="price-box">
                                                                <span class="regular-price">
                                                                    <span class="price">
                                                                        <span class="price1"><?php echo $row->price.'/-'; ?></span>
                                                                        <!-- <span class="price2">600.00</span> -->
                                                                    </span>
                                                                </span>
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="action-bot">
                                                             <div class="wrap-addtocart">
                                                                 <?php if(empty($_SESSION['id'])){ ?>
                                                                <a href="<?php echo site_url("Login"); ?>">
                                                                 <?php }else{ ?>
                                                                <a href="javascript:void(0);">
                                                                 <?php } ?>
                                                                 <button class="btn-cart addToCart" value="<?php echo $row->service_id; ?>"
                                                                         title="Add to Cart">
                                                                     <i class="fa fa-shopping-cart"></i>
                                                                     <span>Add to Cart</span>
                                                                 </button>
                                                                 </a>
                                                             </div>
                                                             <div class="actions">
                                                                 <ul class="add-to-links">
                                                                     <li>
                                                                         <a class="link-wishlist"
                                                                            href="#"
                                                                            title="Add to Wishlist">
                                                                             <i class="fa fa-heart"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li>
                                                                         <a class="link-compare"
                                                                            href="#"
                                                                            title="Add to Compare">
                                                                             <i class="fa fa-random"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li class="wrap-quickview" data-id="qv_item_7">
                                                                         <div class="quickview-wrap">
                                                                             <a class="sns-btn-quickview qv_btn"
                                                                                href="#">
                                                                                 <i class="fa fa-eye"></i>
                                                                             </a>
                                                                         </div>
                                                                     </li>
                                                                 </ul>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>
                                        <?php } ?>
                                        </div>
                                    </div>




                                    <div role="tabpanel" class="tab-pane" id="profile">
                                        <div class="products-grid row style_grid">
                                            <div class="item col-lg-2d4 col-md-3 col-sm-4 col-xs-6 col-phone-12">
                                                 <div class="item-inner">
                                                     <div class="prd">
                                                         <div class="item-img clearfix">
                                                             <div class="ico-label"></div>
                                                             <a class="product-image have-additional"
                                                                title="Modular Modern"
                                                                href="index3-detail.html">
                                                                <span class="img-main">
                                                               <img src="<?php echo base_url().'assets/images/products/30.jpg';?>" alt="">
                                                                </span>
                                                             </a>
                                                         </div>
                                                         <div class="item-info">
                                                             <div class="info-inner">
                                                                 <div class="item-title">
                                                                     <a title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                         Modular Modern </a>
                                                                 </div>
                                                                 <div class="item-price">
                                                                     <div class="price-box">
                                                                <span class="regular-price">
                                                                    <span class="price">
                                                                        <span class="price1">$ 540.00</span>
                                                                        <span class="price2">$ 600.00</span>
                                                                    </span>
                                                                </span>
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="action-bot">
                                                             <div class="wrap-addtocart">
                                                                 <button class="btn-cart"
                                                                         title="Add to Cart">
                                                                     <i class="fa fa-shopping-cart"></i>
                                                                     <span>Add to Cart</span>
                                                                 </button>
                                                             </div>
                                                             <div class="actions">
                                                                 <ul class="add-to-links">
                                                                     <li>
                                                                         <a class="link-wishlist"
                                                                            href="#"
                                                                            title="Add to Wishlist">
                                                                             <i class="fa fa-heart"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li>
                                                                         <a class="link-compare"
                                                                            href="#"
                                                                            title="Add to Compare">
                                                                             <i class="fa fa-random"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li class="wrap-quickview" data-id="qv_item_7">
                                                                         <div class="quickview-wrap">
                                                                             <a class="sns-btn-quickview qv_btn"
                                                                                href="#">
                                                                                 <i class="fa fa-eye"></i>
                                                                             </a>
                                                                         </div>
                                                                     </li>
                                                                 </ul>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>
                                             <div class="item col-lg-2d4 col-md-3 col-sm-4 col-xs-6 col-phone-12">
                                                 <div class="item-inner">
                                                     <div class="prd">
                                                         <div class="item-img clearfix">
                                                             <div class="ico-label"></div>
                                                             <a class="product-image have-additional"
                                                                title="Modular Modern"
                                                                href="index3-detail.html">
                                                                <span class="img-main">
                                                               <img src="<?php echo base_url().'assets/images/products/29.jpg';?>" alt="">
                                                                </span>
                                                             </a>
                                                         </div>
                                                         <div class="item-info">
                                                             <div class="info-inner">
                                                                 <div class="item-title">
                                                                     <a title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                         Modular Modern </a>
                                                                 </div>
                                                                 <div class="item-price">
                                                                     <div class="price-box">
                                                                <span class="regular-price">
                                                                    <span class="price">
                                                                        <span class="price1">$ 540.00</span>
                                                                    </span>
                                                                </span>
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="action-bot">
                                                             <div class="wrap-addtocart">
                                                                 <button class="btn-cart"
                                                                         title="Add to Cart">
                                                                     <i class="fa fa-shopping-cart"></i>
                                                                     <span>Add to Cart</span>
                                                                 </button>
                                                             </div>
                                                             <div class="actions">
                                                                 <ul class="add-to-links">
                                                                     <li>
                                                                         <a class="link-wishlist"
                                                                            href="#"
                                                                            title="Add to Wishlist">
                                                                             <i class="fa fa-heart"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li>
                                                                         <a class="link-compare"
                                                                            href="#"
                                                                            title="Add to Compare">
                                                                             <i class="fa fa-random"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li class="wrap-quickview" data-id="qv_item_7">
                                                                         <div class="quickview-wrap">
                                                                             <a class="sns-btn-quickview qv_btn"
                                                                                href="#">
                                                                                 <i class="fa fa-eye"></i>
                                                                             </a>
                                                                         </div>
                                                                     </li>
                                                                 </ul>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>

                                             <div class="item col-lg-2d4 col-md-3 col-sm-4 col-xs-6 col-phone-12">
                                                 <div class="item-inner">
                                                     <div class="prd">
                                                         <div class="item-img clearfix">
                                                             <div class="ico-label"></div>
                                                             <a class="product-image have-additional"
                                                                title="Modular Modern"
                                                                href="index3-detail.html">
                                                                <span class="img-main">
                                                               <img src="<?php echo base_url().'assets/images/products/28.jpg';?>" alt="">
                                                                </span>
                                                             </a>
                                                         </div>
                                                         <div class="item-info">
                                                             <div class="info-inner">
                                                                 <div class="item-title">
                                                                     <a title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                         Modular Modern </a>
                                                                 </div>
                                                                 <div class="item-price">
                                                                     <div class="price-box">
                                                                <span class="regular-price">
                                                                    <span class="price">
                                                                        <span class="price1">$ 540.00</span>
                                                                    </span>
                                                                </span>
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="action-bot">
                                                             <div class="wrap-addtocart">
                                                                 <button class="btn-cart"
                                                                         title="Add to Cart">
                                                                     <i class="fa fa-shopping-cart"></i>
                                                                     <span>Add to Cart</span>
                                                                 </button>
                                                             </div>
                                                             <div class="actions">
                                                                 <ul class="add-to-links">
                                                                     <li>
                                                                         <a class="link-wishlist"
                                                                            href="#"
                                                                            title="Add to Wishlist">
                                                                             <i class="fa fa-heart"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li>
                                                                         <a class="link-compare"
                                                                            href="#"
                                                                            title="Add to Compare">
                                                                             <i class="fa fa-random"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li class="wrap-quickview" data-id="qv_item_7">
                                                                         <div class="quickview-wrap">
                                                                             <a class="sns-btn-quickview qv_btn"
                                                                                href="#">
                                                                                 <i class="fa fa-eye"></i>
                                                                             </a>
                                                                         </div>
                                                                     </li>
                                                                 </ul>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>

                                             <div class="item col-lg-2d4 col-md-3 col-sm-4 col-xs-6 col-phone-12">
                                                 <div class="item-inner">
                                                     <div class="prd">
                                                         <div class="item-img clearfix">
                                                             <div class="ico-label"></div>
                                                             <a class="product-image have-additional"
                                                                title="Modular Modern"
                                                                href="index3-detail.html">
                                                                <span class="img-main">
                                                               <img src="<?php echo base_url().'assets/images/products/27.jpg';?>" alt="">
                                                                </span>
                                                             </a>
                                                         </div>
                                                         <div class="item-info">
                                                             <div class="info-inner">
                                                                 <div class="item-title">
                                                                     <a title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                         Modular Modern </a>
                                                                 </div>
                                                                 <div class="item-price">
                                                                     <div class="price-box">
                                                                <span class="regular-price">
                                                                    <span class="price">
                                                                        <span class="price1">$ 540.00</span>
                                                                    </span>
                                                                </span>
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="action-bot">
                                                             <div class="wrap-addtocart">
                                                                 <button class="btn-cart"
                                                                         title="Add to Cart">
                                                                     <i class="fa fa-shopping-cart"></i>
                                                                     <span>Add to Cart</span>
                                                                 </button>
                                                             </div>
                                                             <div class="actions">
                                                                 <ul class="add-to-links">
                                                                     <li>
                                                                         <a class="link-wishlist"
                                                                            href="#"
                                                                            title="Add to Wishlist">
                                                                             <i class="fa fa-heart"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li>
                                                                         <a class="link-compare"
                                                                            href="#"
                                                                            title="Add to Compare">
                                                                             <i class="fa fa-random"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li class="wrap-quickview" data-id="qv_item_7">
                                                                         <div class="quickview-wrap">
                                                                             <a class="sns-btn-quickview qv_btn"
                                                                                href="#">
                                                                                 <i class="fa fa-eye"></i>
                                                                             </a>
                                                                         </div>
                                                                     </li>
                                                                 </ul>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>

                                             <div class="item col-lg-2d4 col-md-3 col-sm-4 col-xs-6 col-phone-12">
                                                 <div class="item-inner">
                                                     <div class="prd">
                                                         <div class="item-img clearfix">
                                                             <div class="ico-label"></div>
                                                             <a class="product-image have-additional"
                                                                title="Modular Modern"
                                                                href="index3-detail.html">
                                                                <span class="img-main">
                                                               <img src="<?php echo base_url().'assets/images/products/26.jpg';?>" alt="">
                                                                </span>
                                                             </a>
                                                         </div>
                                                         <div class="item-info">
                                                             <div class="info-inner">
                                                                 <div class="item-title">
                                                                     <a title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                         Modular Modern </a>
                                                                 </div>
                                                                 <div class="item-price">
                                                                     <div class="price-box">
                                                                <span class="regular-price">
                                                                    <span class="price">
                                                                        <span class="price1">$ 540.00</span>
                                                                    </span>
                                                                </span>
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="action-bot">
                                                             <div class="wrap-addtocart">
                                                                 <button class="btn-cart"
                                                                         title="Add to Cart">
                                                                     <i class="fa fa-shopping-cart"></i>
                                                                     <span>Add to Cart</span>
                                                                 </button>
                                                             </div>
                                                             <div class="actions">
                                                                 <ul class="add-to-links">
                                                                     <li>
                                                                         <a class="link-wishlist"
                                                                            href="#"
                                                                            title="Add to Wishlist">
                                                                             <i class="fa fa-heart"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li>
                                                                         <a class="link-compare"
                                                                            href="#"
                                                                            title="Add to Compare">
                                                                             <i class="fa fa-random"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li class="wrap-quickview" data-id="qv_item_7">
                                                                         <div class="quickview-wrap">
                                                                             <a class="sns-btn-quickview qv_btn"
                                                                                href="#">
                                                                                 <i class="fa fa-eye"></i>
                                                                             </a>
                                                                         </div>
                                                                     </li>
                                                                 </ul>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>

                                             <div class="item col-lg-2d4 col-md-3 col-sm-4 col-xs-6 col-phone-12">
                                                 <div class="item-inner">
                                                     <div class="prd">
                                                         <div class="item-img clearfix">
                                                             <div class="ico-label"></div>
                                                             <a class="product-image have-additional"
                                                                title="Modular Modern"
                                                                href="index3-detail.html">
                                                                <span class="img-main">
                                                               <img src="<?php echo base_url().'assets/images/products/25.jpg';?>" alt="">
                                                                </span>
                                                             </a>
                                                         </div>
                                                         <div class="item-info">
                                                             <div class="info-inner">
                                                                 <div class="item-title">
                                                                     <a title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                         Modular Modern </a>
                                                                 </div>
                                                                 <div class="item-price">
                                                                     <div class="price-box">
                                                                <span class="regular-price">
                                                                    <span class="price">
                                                                        <span class="price1">$ 540.00</span>
                                                                        <span class="price2">$ 600.00</span>
                                                                    </span>
                                                                </span>
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="action-bot">
                                                             <div class="wrap-addtocart">
                                                                 <button class="btn-cart"
                                                                         title="Add to Cart">
                                                                     <i class="fa fa-shopping-cart"></i>
                                                                     <span>Add to Cart</span>
                                                                 </button>
                                                             </div>
                                                             <div class="actions">
                                                                 <ul class="add-to-links">
                                                                     <li>
                                                                         <a class="link-wishlist"
                                                                            href="#"
                                                                            title="Add to Wishlist">
                                                                             <i class="fa fa-heart"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li>
                                                                         <a class="link-compare"
                                                                            href="#"
                                                                            title="Add to Compare">
                                                                             <i class="fa fa-random"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li class="wrap-quickview" data-id="qv_item_7">
                                                                         <div class="quickview-wrap">
                                                                             <a class="sns-btn-quickview qv_btn"
                                                                                href="#">
                                                                                 <i class="fa fa-eye"></i>
                                                                             </a>
                                                                         </div>
                                                                     </li>
                                                                 </ul>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>

                                             <div class="item col-lg-2d4 col-md-3 col-sm-4 col-xs-6 col-phone-12">
                                                 <div class="item-inner">
                                                     <div class="prd">
                                                         <div class="item-img clearfix">
                                                             <div class="ico-label"></div>
                                                             <a class="product-image have-additional"
                                                                title="Modular Modern"
                                                                href="index3-detail.html">
                                                                <span class="img-main">
                                                               <img src="<?php echo base_url().'assets/images/products/24.jpg';?>" alt="">
                                                                </span>
                                                             </a>
                                                         </div>
                                                         <div class="item-info">
                                                             <div class="info-inner">
                                                                 <div class="item-title">
                                                                     <a title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                         Modular Modern </a>
                                                                 </div>
                                                                 <div class="item-price">
                                                                     <div class="price-box">
                                                                <span class="regular-price">
                                                                    <span class="price">
                                                                        <span class="price1">$ 540.00</span>
                                                                    </span>
                                                                </span>
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="action-bot">
                                                             <div class="wrap-addtocart">
                                                                 <button class="btn-cart"
                                                                         title="Add to Cart">
                                                                     <i class="fa fa-shopping-cart"></i>
                                                                     <span>Add to Cart</span>
                                                                 </button>
                                                             </div>
                                                             <div class="actions">
                                                                 <ul class="add-to-links">
                                                                     <li>
                                                                         <a class="link-wishlist"
                                                                            href="#"
                                                                            title="Add to Wishlist">
                                                                             <i class="fa fa-heart"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li>
                                                                         <a class="link-compare"
                                                                            href="#"
                                                                            title="Add to Compare">
                                                                             <i class="fa fa-random"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li class="wrap-quickview" data-id="qv_item_7">
                                                                         <div class="quickview-wrap">
                                                                             <a class="sns-btn-quickview qv_btn"
                                                                                href="#">
                                                                                 <i class="fa fa-eye"></i>
                                                                             </a>
                                                                         </div>
                                                                     </li>
                                                                 </ul>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>

                                             <div class="item col-lg-2d4 col-md-3 col-sm-4 col-xs-6 col-phone-12">
                                                 <div class="item-inner">
                                                     <div class="prd">
                                                         <div class="item-img clearfix">
                                                             <div class="ico-label"></div>
                                                             <a class="product-image have-additional"
                                                                title="Modular Modern"
                                                                href="index3-detail.html">
                                                                <span class="img-main">
                                                               <img src="<?php echo base_url().'assets/images/products/23.jpg';?>" alt="">
                                                                </span>
                                                             </a>
                                                         </div>
                                                         <div class="item-info">
                                                             <div class="info-inner">
                                                                 <div class="item-title">
                                                                     <a title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                         Modular Modern </a>
                                                                 </div>
                                                                 <div class="item-price">
                                                                     <div class="price-box">
                                                                <span class="regular-price">
                                                                    <span class="price">
                                                                        <span class="price1">$ 540.00</span>
                                                                    </span>
                                                                </span>
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="action-bot">
                                                             <div class="wrap-addtocart">
                                                                 <button class="btn-cart"
                                                                         title="Add to Cart">
                                                                     <i class="fa fa-shopping-cart"></i>
                                                                     <span>Add to Cart</span>
                                                                 </button>
                                                             </div>
                                                             <div class="actions">
                                                                 <ul class="add-to-links">
                                                                     <li>
                                                                         <a class="link-wishlist"
                                                                            href="#"
                                                                            title="Add to Wishlist">
                                                                             <i class="fa fa-heart"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li>
                                                                         <a class="link-compare"
                                                                            href="#"
                                                                            title="Add to Compare">
                                                                             <i class="fa fa-random"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li class="wrap-quickview" data-id="qv_item_7">
                                                                         <div class="quickview-wrap">
                                                                             <a class="sns-btn-quickview qv_btn"
                                                                                href="#">
                                                                                 <i class="fa fa-eye"></i>
                                                                             </a>
                                                                         </div>
                                                                     </li>
                                                                 </ul>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>

                                             <div class="item col-lg-2d4 col-md-3 col-sm-4 col-xs-6 col-phone-12">
                                                 <div class="item-inner">
                                                     <div class="prd">
                                                         <div class="item-img clearfix">
                                                             <div class="ico-label"></div>
                                                             <a class="product-image have-additional"
                                                                title="Modular Modern"
                                                                href="index3-detail.html">
                                                                <span class="img-main">
                                                               <img src="<?php echo base_url().'assets/images/products/22.jpg';?>" alt="">
                                                                </span>
                                                             </a>
                                                         </div>
                                                         <div class="item-info">
                                                             <div class="info-inner">
                                                                 <div class="item-title">
                                                                     <a title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                         Modular Modern </a>
                                                                 </div>
                                                                 <div class="item-price">
                                                                     <div class="price-box">
                                                                <span class="regular-price">
                                                                    <span class="price">
                                                                        <span class="price1">$ 540.00</span>
                                                                    </span>
                                                                </span>
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="action-bot">
                                                             <div class="wrap-addtocart">
                                                                 <button class="btn-cart"
                                                                         title="Add to Cart">
                                                                     <i class="fa fa-shopping-cart"></i>
                                                                     <span>Add to Cart</span>
                                                                 </button>
                                                             </div>
                                                             <div class="actions">
                                                                 <ul class="add-to-links">
                                                                     <li>
                                                                         <a class="link-wishlist"
                                                                            href="#"
                                                                            title="Add to Wishlist">
                                                                             <i class="fa fa-heart"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li>
                                                                         <a class="link-compare"
                                                                            href="#"
                                                                            title="Add to Compare">
                                                                             <i class="fa fa-random"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li class="wrap-quickview" data-id="qv_item_7">
                                                                         <div class="quickview-wrap">
                                                                             <a class="sns-btn-quickview qv_btn"
                                                                                href="#">
                                                                                 <i class="fa fa-eye"></i>
                                                                             </a>
                                                                         </div>
                                                                     </li>
                                                                 </ul>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>

                                             <div class="item col-lg-2d4 col-md-3 col-sm-4 col-xs-6 col-phone-12">
                                                 <div class="item-inner">
                                                     <div class="prd">
                                                         <div class="item-img clearfix">
                                                             <div class="ico-label"></div>
                                                             <a class="product-image have-additional"
                                                                title="Modular Modern"
                                                                href="index3-detail.html">
                                                                <span class="img-main">
                                                               <img src="<?php echo base_url().'assets/images/products/21.jpg';?>" alt="">
                                                                </span>
                                                             </a>
                                                         </div>
                                                         <div class="item-info">
                                                             <div class="info-inner">
                                                                 <div class="item-title">
                                                                     <a title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                         Modular Modern </a>
                                                                 </div>
                                                                 <div class="item-price">
                                                                     <div class="price-box">
                                                                <span class="regular-price">
                                                                    <span class="price">
                                                                        <span class="price1">$ 540.00</span>
                                                                    </span>
                                                                </span>
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="action-bot">
                                                             <div class="wrap-addtocart">
                                                                 <button class="btn-cart"
                                                                         title="Add to Cart">
                                                                     <i class="fa fa-shopping-cart"></i>
                                                                     <span>Add to Cart</span>
                                                                 </button>
                                                             </div>
                                                             <div class="actions">
                                                                 <ul class="add-to-links">
                                                                     <li>
                                                                         <a class="link-wishlist"
                                                                            href="#"
                                                                            title="Add to Wishlist">
                                                                             <i class="fa fa-heart"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li>
                                                                         <a class="link-compare"
                                                                            href="#"
                                                                            title="Add to Compare">
                                                                             <i class="fa fa-random"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li class="wrap-quickview" data-id="qv_item_7">
                                                                         <div class="quickview-wrap">
                                                                             <a class="sns-btn-quickview qv_btn"
                                                                                href="#">
                                                                                 <i class="fa fa-eye"></i>
                                                                             </a>
                                                                         </div>
                                                                     </li>
                                                                 </ul>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>
                                        </div>
                                    </div>


                                    <div role="tabpanel" class="tab-pane" id="messages">
                                        <div class="products-grid row style_grid">
                                            <div class="item col-lg-2d4 col-md-3 col-sm-4 col-xs-6 col-phone-12">
                                                 <div class="item-inner">
                                                     <div class="prd">
                                                         <div class="item-img clearfix">
                                                             <div class="ico-label"></div>
                                                             <a class="product-image have-additional"
                                                                title="Modular Modern"
                                                                href="index3-detail.html">
                                                                <span class="img-main">
                                                               <img src="<?php echo base_url().'assets/images/products/3.jpg';?>" alt="">
                                                                </span>
                                                             </a>
                                                         </div>
                                                         <div class="item-info">
                                                             <div class="info-inner">
                                                                 <div class="item-title">
                                                                     <a title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                         Modular Modern </a>
                                                                 </div>
                                                                 <div class="item-price">
                                                                     <div class="price-box">
                                                                <span class="regular-price">
                                                                    <span class="price">
                                                                        <span class="price1">$ 540.00</span>
                                                                        <span class="price2">$ 600.00</span>
                                                                    </span>
                                                                </span>
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="action-bot">
                                                             <div class="wrap-addtocart">
                                                                 <button class="btn-cart"
                                                                         title="Add to Cart">
                                                                     <i class="fa fa-shopping-cart"></i>
                                                                     <span>Add to Cart</span>
                                                                 </button>
                                                             </div>
                                                             <div class="actions">
                                                                 <ul class="add-to-links">
                                                                     <li>
                                                                         <a class="link-wishlist"
                                                                            href="#"
                                                                            title="Add to Wishlist">
                                                                             <i class="fa fa-heart"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li>
                                                                         <a class="link-compare"
                                                                            href="#"
                                                                            title="Add to Compare">
                                                                             <i class="fa fa-random"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li class="wrap-quickview" data-id="qv_item_7">
                                                                         <div class="quickview-wrap">
                                                                             <a class="sns-btn-quickview qv_btn"
                                                                                href="#">
                                                                                 <i class="fa fa-eye"></i>
                                                                             </a>
                                                                         </div>
                                                                     </li>
                                                                 </ul>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>
                                             <div class="item col-lg-2d4 col-md-3 col-sm-4 col-xs-6 col-phone-12">
                                                 <div class="item-inner">
                                                     <div class="prd">
                                                         <div class="item-img clearfix">
                                                             <div class="ico-label"></div>
                                                             <a class="product-image have-additional"
                                                                title="Modular Modern"
                                                                href="index3-detail.html">
                                                                <span class="img-main">
                                                               <img src="<?php echo base_url().'assets/images/products/5.jpg';?>" alt="">
                                                                </span>
                                                             </a>
                                                         </div>
                                                         <div class="item-info">
                                                             <div class="info-inner">
                                                                 <div class="item-title">
                                                                     <a title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                         Modular Modern </a>
                                                                 </div>
                                                                 <div class="item-price">
                                                                     <div class="price-box">
                                                                <span class="regular-price">
                                                                    <span class="price">
                                                                        <span class="price1">$ 540.00</span>
                                                                    </span>
                                                                </span>
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="action-bot">
                                                             <div class="wrap-addtocart">
                                                                 <button class="btn-cart"
                                                                         title="Add to Cart">
                                                                     <i class="fa fa-shopping-cart"></i>
                                                                     <span>Add to Cart</span>
                                                                 </button>
                                                             </div>
                                                             <div class="actions">
                                                                 <ul class="add-to-links">
                                                                     <li>
                                                                         <a class="link-wishlist"
                                                                            href="#"
                                                                            title="Add to Wishlist">
                                                                             <i class="fa fa-heart"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li>
                                                                         <a class="link-compare"
                                                                            href="#"
                                                                            title="Add to Compare">
                                                                             <i class="fa fa-random"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li class="wrap-quickview" data-id="qv_item_7">
                                                                         <div class="quickview-wrap">
                                                                             <a class="sns-btn-quickview qv_btn"
                                                                                href="#">
                                                                                 <i class="fa fa-eye"></i>
                                                                             </a>
                                                                         </div>
                                                                     </li>
                                                                 </ul>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>

                                             <div class="item col-lg-2d4 col-md-3 col-sm-4 col-xs-6 col-phone-12">
                                                 <div class="item-inner">
                                                     <div class="prd">
                                                         <div class="item-img clearfix">
                                                             <div class="ico-label"></div>
                                                             <a class="product-image have-additional"
                                                                title="Modular Modern"
                                                                href="index3-detail.html">
                                                                <span class="img-main">
                                                               <img src="<?php echo base_url().'assets/images/products/7.jpg';?>" alt="">
                                                                </span>
                                                             </a>
                                                         </div>
                                                         <div class="item-info">
                                                             <div class="info-inner">
                                                                 <div class="item-title">
                                                                     <a title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                         Modular Modern </a>
                                                                 </div>
                                                                 <div class="item-price">
                                                                     <div class="price-box">
                                                                <span class="regular-price">
                                                                    <span class="price">
                                                                        <span class="price1">$ 540.00</span>
                                                                    </span>
                                                                </span>
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="action-bot">
                                                             <div class="wrap-addtocart">
                                                                 <button class="btn-cart"
                                                                         title="Add to Cart">
                                                                     <i class="fa fa-shopping-cart"></i>
                                                                     <span>Add to Cart</span>
                                                                 </button>
                                                             </div>
                                                             <div class="actions">
                                                                 <ul class="add-to-links">
                                                                     <li>
                                                                         <a class="link-wishlist"
                                                                            href="#"
                                                                            title="Add to Wishlist">
                                                                             <i class="fa fa-heart"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li>
                                                                         <a class="link-compare"
                                                                            href="#"
                                                                            title="Add to Compare">
                                                                             <i class="fa fa-random"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li class="wrap-quickview" data-id="qv_item_7">
                                                                         <div class="quickview-wrap">
                                                                             <a class="sns-btn-quickview qv_btn"
                                                                                href="#">
                                                                                 <i class="fa fa-eye"></i>
                                                                             </a>
                                                                         </div>
                                                                     </li>
                                                                 </ul>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>

                                             <div class="item col-lg-2d4 col-md-3 col-sm-4 col-xs-6 col-phone-12">
                                                 <div class="item-inner">
                                                     <div class="prd">
                                                         <div class="item-img clearfix">
                                                             <div class="ico-label"></div>
                                                             <a class="product-image have-additional"
                                                                title="Modular Modern"
                                                                href="index3-detail.html">
                                                                <span class="img-main">
                                                               <img src="<?php echo base_url().'assets/images/products/9.jpg';?>" alt="">
                                                                </span>
                                                             </a>
                                                         </div>
                                                         <div class="item-info">
                                                             <div class="info-inner">
                                                                 <div class="item-title">
                                                                     <a title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                         Modular Modern </a>
                                                                 </div>
                                                                 <div class="item-price">
                                                                     <div class="price-box">
                                                                <span class="regular-price">
                                                                    <span class="price">
                                                                        <span class="price1">$ 540.00</span>
                                                                    </span>
                                                                </span>
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="action-bot">
                                                             <div class="wrap-addtocart">
                                                                 <button class="btn-cart"
                                                                         title="Add to Cart">
                                                                     <i class="fa fa-shopping-cart"></i>
                                                                     <span>Add to Cart</span>
                                                                 </button>
                                                             </div>
                                                             <div class="actions">
                                                                 <ul class="add-to-links">
                                                                     <li>
                                                                         <a class="link-wishlist"
                                                                            href="#"
                                                                            title="Add to Wishlist">
                                                                             <i class="fa fa-heart"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li>
                                                                         <a class="link-compare"
                                                                            href="#"
                                                                            title="Add to Compare">
                                                                             <i class="fa fa-random"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li class="wrap-quickview" data-id="qv_item_7">
                                                                         <div class="quickview-wrap">
                                                                             <a class="sns-btn-quickview qv_btn"
                                                                                href="#">
                                                                                 <i class="fa fa-eye"></i>
                                                                             </a>
                                                                         </div>
                                                                     </li>
                                                                 </ul>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>

                                             <div class="item col-lg-2d4 col-md-3 col-sm-4 col-xs-6 col-phone-12">
                                                 <div class="item-inner">
                                                     <div class="prd">
                                                         <div class="item-img clearfix">
                                                             <div class="ico-label"></div>
                                                             <a class="product-image have-additional"
                                                                title="Modular Modern"
                                                                href="index3-detail.html">
                                                                <span class="img-main">
                                                               <img src="<?php echo base_url().'assets/images/products/11.jpg';?>" alt="">
                                                                </span>
                                                             </a>
                                                         </div>
                                                         <div class="item-info">
                                                             <div class="info-inner">
                                                                 <div class="item-title">
                                                                     <a title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                         Modular Modern </a>
                                                                 </div>
                                                                 <div class="item-price">
                                                                     <div class="price-box">
                                                                <span class="regular-price">
                                                                    <span class="price">
                                                                        <span class="price1">$ 540.00</span>
                                                                    </span>
                                                                </span>
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="action-bot">
                                                             <div class="wrap-addtocart">
                                                                 <button class="btn-cart"
                                                                         title="Add to Cart">
                                                                     <i class="fa fa-shopping-cart"></i>
                                                                     <span>Add to Cart</span>
                                                                 </button>
                                                             </div>
                                                             <div class="actions">
                                                                 <ul class="add-to-links">
                                                                     <li>
                                                                         <a class="link-wishlist"
                                                                            href="#"
                                                                            title="Add to Wishlist">
                                                                             <i class="fa fa-heart"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li>
                                                                         <a class="link-compare"
                                                                            href="#"
                                                                            title="Add to Compare">
                                                                             <i class="fa fa-random"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li class="wrap-quickview" data-id="qv_item_7">
                                                                         <div class="quickview-wrap">
                                                                             <a class="sns-btn-quickview qv_btn"
                                                                                href="#">
                                                                                 <i class="fa fa-eye"></i>
                                                                             </a>
                                                                         </div>
                                                                     </li>
                                                                 </ul>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>

                                             <div class="item col-lg-2d4 col-md-3 col-sm-4 col-xs-6 col-phone-12">
                                                 <div class="item-inner">
                                                     <div class="prd">
                                                         <div class="item-img clearfix">
                                                             <div class="ico-label"></div>
                                                             <a class="product-image have-additional"
                                                                title="Modular Modern"
                                                                href="index3-detail.html">
                                                                <span class="img-main">
                                                               <img src="<?php echo base_url().'assets/images/products/13.jpg';?>" alt="">
                                                                </span>
                                                             </a>
                                                         </div>
                                                         <div class="item-info">
                                                             <div class="info-inner">
                                                                 <div class="item-title">
                                                                     <a title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                         Modular Modern </a>
                                                                 </div>
                                                                 <div class="item-price">
                                                                     <div class="price-box">
                                                                <span class="regular-price">
                                                                    <span class="price">
                                                                        <span class="price1">$ 540.00</span>
                                                                        <span class="price2">$ 600.00</span>
                                                                    </span>
                                                                </span>
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="action-bot">
                                                             <div class="wrap-addtocart">
                                                                 <button class="btn-cart"
                                                                         title="Add to Cart">
                                                                     <i class="fa fa-shopping-cart"></i>
                                                                     <span>Add to Cart</span>
                                                                 </button>
                                                             </div>
                                                             <div class="actions">
                                                                 <ul class="add-to-links">
                                                                     <li>
                                                                         <a class="link-wishlist"
                                                                            href="#"
                                                                            title="Add to Wishlist">
                                                                             <i class="fa fa-heart"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li>
                                                                         <a class="link-compare"
                                                                            href="#"
                                                                            title="Add to Compare">
                                                                             <i class="fa fa-random"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li class="wrap-quickview" data-id="qv_item_7">
                                                                         <div class="quickview-wrap">
                                                                             <a class="sns-btn-quickview qv_btn"
                                                                                href="#">
                                                                                 <i class="fa fa-eye"></i>
                                                                             </a>
                                                                         </div>
                                                                     </li>
                                                                 </ul>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>

                                             <div class="item col-lg-2d4 col-md-3 col-sm-4 col-xs-6 col-phone-12">
                                                 <div class="item-inner">
                                                     <div class="prd">
                                                         <div class="item-img clearfix">
                                                             <div class="ico-label"></div>
                                                             <a class="product-image have-additional"
                                                                title="Modular Modern"
                                                                href="index3-detail.html">
                                                                <span class="img-main">
                                                               <img src="<?php echo base_url().'assets/images/products/15.jpg';?>" alt="">
                                                                </span>
                                                             </a>
                                                         </div>
                                                         <div class="item-info">
                                                             <div class="info-inner">
                                                                 <div class="item-title">
                                                                     <a title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                         Modular Modern </a>
                                                                 </div>
                                                                 <div class="item-price">
                                                                     <div class="price-box">
                                                                <span class="regular-price">
                                                                    <span class="price">
                                                                        <span class="price1">$ 540.00</span>
                                                                    </span>
                                                                </span>
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="action-bot">
                                                             <div class="wrap-addtocart">
                                                                 <button class="btn-cart"
                                                                         title="Add to Cart">
                                                                     <i class="fa fa-shopping-cart"></i>
                                                                     <span>Add to Cart</span>
                                                                 </button>
                                                             </div>
                                                             <div class="actions">
                                                                 <ul class="add-to-links">
                                                                     <li>
                                                                         <a class="link-wishlist"
                                                                            href="#"
                                                                            title="Add to Wishlist">
                                                                             <i class="fa fa-heart"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li>
                                                                         <a class="link-compare"
                                                                            href="#"
                                                                            title="Add to Compare">
                                                                             <i class="fa fa-random"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li class="wrap-quickview" data-id="qv_item_7">
                                                                         <div class="quickview-wrap">
                                                                             <a class="sns-btn-quickview qv_btn"
                                                                                href="#">
                                                                                 <i class="fa fa-eye"></i>
                                                                             </a>
                                                                         </div>
                                                                     </li>
                                                                 </ul>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>

                                             <div class="item col-lg-2d4 col-md-3 col-sm-4 col-xs-6 col-phone-12">
                                                 <div class="item-inner">
                                                     <div class="prd">
                                                         <div class="item-img clearfix">
                                                             <div class="ico-label"></div>
                                                             <a class="product-image have-additional"
                                                                title="Modular Modern"
                                                                href="index3-detail.html">
                                                                <span class="img-main">
                                                               <img src="<?php echo base_url().'assets/images/products/17.jpg';?>" alt="">
                                                                </span>
                                                             </a>
                                                         </div>
                                                         <div class="item-info">
                                                             <div class="info-inner">
                                                                 <div class="item-title">
                                                                     <a title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                         Modular Modern </a>
                                                                 </div>
                                                                 <div class="item-price">
                                                                     <div class="price-box">
                                                                <span class="regular-price">
                                                                    <span class="price">
                                                                        <span class="price1">$ 540.00</span>
                                                                    </span>
                                                                </span>
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="action-bot">
                                                             <div class="wrap-addtocart">
                                                                 <button class="btn-cart"
                                                                         title="Add to Cart">
                                                                     <i class="fa fa-shopping-cart"></i>
                                                                     <span>Add to Cart</span>
                                                                 </button>
                                                             </div>
                                                             <div class="actions">
                                                                 <ul class="add-to-links">
                                                                     <li>
                                                                         <a class="link-wishlist"
                                                                            href="#"
                                                                            title="Add to Wishlist">
                                                                             <i class="fa fa-heart"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li>
                                                                         <a class="link-compare"
                                                                            href="#"
                                                                            title="Add to Compare">
                                                                             <i class="fa fa-random"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li class="wrap-quickview" data-id="qv_item_7">
                                                                         <div class="quickview-wrap">
                                                                             <a class="sns-btn-quickview qv_btn"
                                                                                href="#">
                                                                                 <i class="fa fa-eye"></i>
                                                                             </a>
                                                                         </div>
                                                                     </li>
                                                                 </ul>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>

                                             <div class="item col-lg-2d4 col-md-3 col-sm-4 col-xs-6 col-phone-12">
                                                 <div class="item-inner">
                                                     <div class="prd">
                                                         <div class="item-img clearfix">
                                                             <div class="ico-label"></div>
                                                             <a class="product-image have-additional"
                                                                title="Modular Modern"
                                                                href="index3-detail.html">
                                                                <span class="img-main">
                                                               <img src="<?php echo base_url().'assets/images/products/19.jpg';?>" alt="">
                                                                </span>
                                                             </a>
                                                         </div>
                                                         <div class="item-info">
                                                             <div class="info-inner">
                                                                 <div class="item-title">
                                                                     <a title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                         Modular Modern </a>
                                                                 </div>
                                                                 <div class="item-price">
                                                                     <div class="price-box">
                                                                <span class="regular-price">
                                                                    <span class="price">
                                                                        <span class="price1">$ 540.00</span>
                                                                    </span>
                                                                </span>
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="action-bot">
                                                             <div class="wrap-addtocart">
                                                                 <button class="btn-cart"
                                                                         title="Add to Cart">
                                                                     <i class="fa fa-shopping-cart"></i>
                                                                     <span>Add to Cart</span>
                                                                 </button>
                                                             </div>
                                                             <div class="actions">
                                                                 <ul class="add-to-links">
                                                                     <li>
                                                                         <a class="link-wishlist"
                                                                            href="#"
                                                                            title="Add to Wishlist">
                                                                             <i class="fa fa-heart"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li>
                                                                         <a class="link-compare"
                                                                            href="#"
                                                                            title="Add to Compare">
                                                                             <i class="fa fa-random"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li class="wrap-quickview" data-id="qv_item_7">
                                                                         <div class="quickview-wrap">
                                                                             <a class="sns-btn-quickview qv_btn"
                                                                                href="#">
                                                                                 <i class="fa fa-eye"></i>
                                                                             </a>
                                                                         </div>
                                                                     </li>
                                                                 </ul>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>

                                             <div class="item col-lg-2d4 col-md-3 col-sm-4 col-xs-6 col-phone-12">
                                                 <div class="item-inner">
                                                     <div class="prd">
                                                         <div class="item-img clearfix">
                                                             <div class="ico-label"></div>
                                                             <a class="product-image have-additional"
                                                                title="Modular Modern"
                                                                href="index3-detail.html">
                                                                <span class="img-main">
                                                               <img src="<?php echo base_url().'assets/images/products/21.jpg';?>" alt="">
                                                                </span>
                                                             </a>
                                                         </div>
                                                         <div class="item-info">
                                                             <div class="info-inner">
                                                                 <div class="item-title">
                                                                     <a title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                         Modular Modern </a>
                                                                 </div>
                                                                 <div class="item-price">
                                                                     <div class="price-box">
                                                                <span class="regular-price">
                                                                    <span class="price">
                                                                        <span class="price1">$ 540.00</span>
                                                                    </span>
                                                                </span>
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="action-bot">
                                                             <div class="wrap-addtocart">
                                                                 <button class="btn-cart"
                                                                         title="Add to Cart">
                                                                     <i class="fa fa-shopping-cart"></i>
                                                                     <span>Add to Cart</span>
                                                                 </button>
                                                             </div>
                                                             <div class="actions">
                                                                 <ul class="add-to-links">
                                                                     <li>
                                                                         <a class="link-wishlist"
                                                                            href="#"
                                                                            title="Add to Wishlist">
                                                                             <i class="fa fa-heart"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li>
                                                                         <a class="link-compare"
                                                                            href="#"
                                                                            title="Add to Compare">
                                                                             <i class="fa fa-random"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li class="wrap-quickview" data-id="qv_item_7">
                                                                         <div class="quickview-wrap">
                                                                             <a class="sns-btn-quickview qv_btn"
                                                                                href="#">
                                                                                 <i class="fa fa-eye"></i>
                                                                             </a>
                                                                         </div>
                                                                     </li>
                                                                 </ul>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>
                                        </div>
                                    </div>

                                    <div role="tabpanel" class="tab-pane" id="bedroom">
                                        <div class="products-grid row style_grid">
                                            <div class="item col-lg-2d4 col-md-3 col-sm-4 col-xs-6 col-phone-12">
                                                 <div class="item-inner">
                                                     <div class="prd">
                                                         <div class="item-img clearfix">
                                                             <div class="ico-label"></div>
                                                             <a class="product-image have-additional"
                                                                title="Modular Modern"
                                                                href="index3-detail.html">
                                                                <span class="img-main">
                                                               <img src="<?php echo base_url().'assets/images/products/30.jpg';?>" alt="">
                                                                </span>
                                                             </a>
                                                         </div>
                                                         <div class="item-info">
                                                             <div class="info-inner">
                                                                 <div class="item-title">
                                                                     <a title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                         Modular Modern </a>
                                                                 </div>
                                                                 <div class="item-price">
                                                                     <div class="price-box">
                                                                <span class="regular-price">
                                                                    <span class="price">
                                                                        <span class="price1">$ 540.00</span>
                                                                        <span class="price2">$ 600.00</span>
                                                                    </span>
                                                                </span>
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="action-bot">
                                                             <div class="wrap-addtocart">
                                                                 <button class="btn-cart"
                                                                         title="Add to Cart">
                                                                     <i class="fa fa-shopping-cart"></i>
                                                                     <span>Add to Cart</span>
                                                                 </button>
                                                             </div>
                                                             <div class="actions">
                                                                 <ul class="add-to-links">
                                                                     <li>
                                                                         <a class="link-wishlist"
                                                                            href="#"
                                                                            title="Add to Wishlist">
                                                                             <i class="fa fa-heart"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li>
                                                                         <a class="link-compare"
                                                                            href="#"
                                                                            title="Add to Compare">
                                                                             <i class="fa fa-random"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li class="wrap-quickview" data-id="qv_item_7">
                                                                         <div class="quickview-wrap">
                                                                             <a class="sns-btn-quickview qv_btn"
                                                                                href="#">
                                                                                 <i class="fa fa-eye"></i>
                                                                             </a>
                                                                         </div>
                                                                     </li>
                                                                 </ul>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>
                                             <div class="item col-lg-2d4 col-md-3 col-sm-4 col-xs-6 col-phone-12">
                                                 <div class="item-inner">
                                                     <div class="prd">
                                                         <div class="item-img clearfix">
                                                             <div class="ico-label"></div>
                                                             <a class="product-image have-additional"
                                                                title="Modular Modern"
                                                                href="index3-detail.html">
                                                                <span class="img-main">
                                                               <img src="<?php echo base_url().'assets/images/products/29.jpg';?>" alt="">
                                                                </span>
                                                             </a>
                                                         </div>
                                                         <div class="item-info">
                                                             <div class="info-inner">
                                                                 <div class="item-title">
                                                                     <a title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                         Modular Modern </a>
                                                                 </div>
                                                                 <div class="item-price">
                                                                     <div class="price-box">
                                                                <span class="regular-price">
                                                                    <span class="price">
                                                                        <span class="price1">$ 540.00</span>
                                                                    </span>
                                                                </span>
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="action-bot">
                                                             <div class="wrap-addtocart">
                                                                 <button class="btn-cart"
                                                                         title="Add to Cart">
                                                                     <i class="fa fa-shopping-cart"></i>
                                                                     <span>Add to Cart</span>
                                                                 </button>
                                                             </div>
                                                             <div class="actions">
                                                                 <ul class="add-to-links">
                                                                     <li>
                                                                         <a class="link-wishlist"
                                                                            href="#"
                                                                            title="Add to Wishlist">
                                                                             <i class="fa fa-heart"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li>
                                                                         <a class="link-compare"
                                                                            href="#"
                                                                            title="Add to Compare">
                                                                             <i class="fa fa-random"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li class="wrap-quickview" data-id="qv_item_7">
                                                                         <div class="quickview-wrap">
                                                                             <a class="sns-btn-quickview qv_btn"
                                                                                href="#">
                                                                                 <i class="fa fa-eye"></i>
                                                                             </a>
                                                                         </div>
                                                                     </li>
                                                                 </ul>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>

                                             <div class="item col-lg-2d4 col-md-3 col-sm-4 col-xs-6 col-phone-12">
                                                 <div class="item-inner">
                                                     <div class="prd">
                                                         <div class="item-img clearfix">
                                                             <div class="ico-label"></div>
                                                             <a class="product-image have-additional"
                                                                title="Modular Modern"
                                                                href="index3-detail.html">
                                                                <span class="img-main">
                                                               <img src="<?php echo base_url().'assets/images/products/28.jpg';?>" alt="">
                                                                </span>
                                                             </a>
                                                         </div>
                                                         <div class="item-info">
                                                             <div class="info-inner">
                                                                 <div class="item-title">
                                                                     <a title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                         Modular Modern </a>
                                                                 </div>
                                                                 <div class="item-price">
                                                                     <div class="price-box">
                                                                <span class="regular-price">
                                                                    <span class="price">
                                                                        <span class="price1">$ 540.00</span>
                                                                    </span>
                                                                </span>
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="action-bot">
                                                             <div class="wrap-addtocart">
                                                                 <button class="btn-cart"
                                                                         title="Add to Cart">
                                                                     <i class="fa fa-shopping-cart"></i>
                                                                     <span>Add to Cart</span>
                                                                 </button>
                                                             </div>
                                                             <div class="actions">
                                                                 <ul class="add-to-links">
                                                                     <li>
                                                                         <a class="link-wishlist"
                                                                            href="#"
                                                                            title="Add to Wishlist">
                                                                             <i class="fa fa-heart"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li>
                                                                         <a class="link-compare"
                                                                            href="#"
                                                                            title="Add to Compare">
                                                                             <i class="fa fa-random"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li class="wrap-quickview" data-id="qv_item_7">
                                                                         <div class="quickview-wrap">
                                                                             <a class="sns-btn-quickview qv_btn"
                                                                                href="#">
                                                                                 <i class="fa fa-eye"></i>
                                                                             </a>
                                                                         </div>
                                                                     </li>
                                                                 </ul>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>

                                             <div class="item col-lg-2d4 col-md-3 col-sm-4 col-xs-6 col-phone-12">
                                                 <div class="item-inner">
                                                     <div class="prd">
                                                         <div class="item-img clearfix">
                                                             <div class="ico-label"></div>
                                                             <a class="product-image have-additional"
                                                                title="Modular Modern"
                                                                href="index3-detail.html">
                                                                <span class="img-main">
                                                               <img src="<?php echo base_url().'assets/images/products/27.jpg';?>" alt="">
                                                                </span>
                                                             </a>
                                                         </div>
                                                         <div class="item-info">
                                                             <div class="info-inner">
                                                                 <div class="item-title">
                                                                     <a title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                         Modular Modern </a>
                                                                 </div>
                                                                 <div class="item-price">
                                                                     <div class="price-box">
                                                                <span class="regular-price">
                                                                    <span class="price">
                                                                        <span class="price1">$ 540.00</span>
                                                                    </span>
                                                                </span>
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="action-bot">
                                                             <div class="wrap-addtocart">
                                                                 <button class="btn-cart"
                                                                         title="Add to Cart">
                                                                     <i class="fa fa-shopping-cart"></i>
                                                                     <span>Add to Cart</span>
                                                                 </button>
                                                             </div>
                                                             <div class="actions">
                                                                 <ul class="add-to-links">
                                                                     <li>
                                                                         <a class="link-wishlist"
                                                                            href="#"
                                                                            title="Add to Wishlist">
                                                                             <i class="fa fa-heart"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li>
                                                                         <a class="link-compare"
                                                                            href="#"
                                                                            title="Add to Compare">
                                                                             <i class="fa fa-random"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li class="wrap-quickview" data-id="qv_item_7">
                                                                         <div class="quickview-wrap">
                                                                             <a class="sns-btn-quickview qv_btn"
                                                                                href="#">
                                                                                 <i class="fa fa-eye"></i>
                                                                             </a>
                                                                         </div>
                                                                     </li>
                                                                 </ul>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>

                                             <div class="item col-lg-2d4 col-md-3 col-sm-4 col-xs-6 col-phone-12">
                                                 <div class="item-inner">
                                                     <div class="prd">
                                                         <div class="item-img clearfix">
                                                             <div class="ico-label"></div>
                                                             <a class="product-image have-additional"
                                                                title="Modular Modern"
                                                                href="index3-detail.html">
                                                                <span class="img-main">
                                                               <img src="<?php echo base_url().'assets/images/products/26.jpg';?>" alt="">
                                                                </span>
                                                             </a>
                                                         </div>
                                                         <div class="item-info">
                                                             <div class="info-inner">
                                                                 <div class="item-title">
                                                                     <a title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                         Modular Modern </a>
                                                                 </div>
                                                                 <div class="item-price">
                                                                     <div class="price-box">
                                                                <span class="regular-price">
                                                                    <span class="price">
                                                                        <span class="price1">$ 540.00</span>
                                                                    </span>
                                                                </span>
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="action-bot">
                                                             <div class="wrap-addtocart">
                                                                 <button class="btn-cart"
                                                                         title="Add to Cart">
                                                                     <i class="fa fa-shopping-cart"></i>
                                                                     <span>Add to Cart</span>
                                                                 </button>
                                                             </div>
                                                             <div class="actions">
                                                                 <ul class="add-to-links">
                                                                     <li>
                                                                         <a class="link-wishlist"
                                                                            href="#"
                                                                            title="Add to Wishlist">
                                                                             <i class="fa fa-heart"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li>
                                                                         <a class="link-compare"
                                                                            href="#"
                                                                            title="Add to Compare">
                                                                             <i class="fa fa-random"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li class="wrap-quickview" data-id="qv_item_7">
                                                                         <div class="quickview-wrap">
                                                                             <a class="sns-btn-quickview qv_btn"
                                                                                href="#">
                                                                                 <i class="fa fa-eye"></i>
                                                                             </a>
                                                                         </div>
                                                                     </li>
                                                                 </ul>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>

                                             <div class="item col-lg-2d4 col-md-3 col-sm-4 col-xs-6 col-phone-12">
                                                 <div class="item-inner">
                                                     <div class="prd">
                                                         <div class="item-img clearfix">
                                                             <div class="ico-label"></div>
                                                             <a class="product-image have-additional"
                                                                title="Modular Modern"
                                                                href="index3-detail.html">
                                                                <span class="img-main">
                                                               <img src="<?php echo base_url().'assets/images/products/25.jpg';?>" alt="">
                                                                </span>
                                                             </a>
                                                         </div>
                                                         <div class="item-info">
                                                             <div class="info-inner">
                                                                 <div class="item-title">
                                                                     <a title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                         Modular Modern </a>
                                                                 </div>
                                                                 <div class="item-price">
                                                                     <div class="price-box">
                                                                <span class="regular-price">
                                                                    <span class="price">
                                                                        <span class="price1">$ 540.00</span>
                                                                        <span class="price2">$ 600.00</span>
                                                                    </span>
                                                                </span>
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="action-bot">
                                                             <div class="wrap-addtocart">
                                                                 <button class="btn-cart"
                                                                         title="Add to Cart">
                                                                     <i class="fa fa-shopping-cart"></i>
                                                                     <span>Add to Cart</span>
                                                                 </button>
                                                             </div>
                                                             <div class="actions">
                                                                 <ul class="add-to-links">
                                                                     <li>
                                                                         <a class="link-wishlist"
                                                                            href="#"
                                                                            title="Add to Wishlist">
                                                                             <i class="fa fa-heart"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li>
                                                                         <a class="link-compare"
                                                                            href="#"
                                                                            title="Add to Compare">
                                                                             <i class="fa fa-random"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li class="wrap-quickview" data-id="qv_item_7">
                                                                         <div class="quickview-wrap">
                                                                             <a class="sns-btn-quickview qv_btn"
                                                                                href="#">
                                                                                 <i class="fa fa-eye"></i>
                                                                             </a>
                                                                         </div>
                                                                     </li>
                                                                 </ul>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>

                                             <div class="item col-lg-2d4 col-md-3 col-sm-4 col-xs-6 col-phone-12">
                                                 <div class="item-inner">
                                                     <div class="prd">
                                                         <div class="item-img clearfix">
                                                             <div class="ico-label"></div>
                                                             <a class="product-image have-additional"
                                                                title="Modular Modern"
                                                                href="index3-detail.html">
                                                                <span class="img-main">
                                                               <img src="<?php echo base_url().'assets/images/products/24.jpg';?>" alt="">
                                                                </span>
                                                             </a>
                                                         </div>
                                                         <div class="item-info">
                                                             <div class="info-inner">
                                                                 <div class="item-title">
                                                                     <a title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                         Modular Modern </a>
                                                                 </div>
                                                                 <div class="item-price">
                                                                     <div class="price-box">
                                                                <span class="regular-price">
                                                                    <span class="price">
                                                                        <span class="price1">$ 540.00</span>
                                                                    </span>
                                                                </span>
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="action-bot">
                                                             <div class="wrap-addtocart">
                                                                 <button class="btn-cart"
                                                                         title="Add to Cart">
                                                                     <i class="fa fa-shopping-cart"></i>
                                                                     <span>Add to Cart</span>
                                                                 </button>
                                                             </div>
                                                             <div class="actions">
                                                                 <ul class="add-to-links">
                                                                     <li>
                                                                         <a class="link-wishlist"
                                                                            href="#"
                                                                            title="Add to Wishlist">
                                                                             <i class="fa fa-heart"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li>
                                                                         <a class="link-compare"
                                                                            href="#"
                                                                            title="Add to Compare">
                                                                             <i class="fa fa-random"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li class="wrap-quickview" data-id="qv_item_7">
                                                                         <div class="quickview-wrap">
                                                                             <a class="sns-btn-quickview qv_btn"
                                                                                href="#">
                                                                                 <i class="fa fa-eye"></i>
                                                                             </a>
                                                                         </div>
                                                                     </li>
                                                                 </ul>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>

                                             <div class="item col-lg-2d4 col-md-3 col-sm-4 col-xs-6 col-phone-12">
                                                 <div class="item-inner">
                                                     <div class="prd">
                                                         <div class="item-img clearfix">
                                                             <div class="ico-label"></div>
                                                             <a class="product-image have-additional"
                                                                title="Modular Modern"
                                                                href="index3-detail.html">
                                                                <span class="img-main">
                                                               <img src="<?php echo base_url().'assets/images/products/23.jpg';?>" alt="">
                                                                </span>
                                                             </a>
                                                         </div>
                                                         <div class="item-info">
                                                             <div class="info-inner">
                                                                 <div class="item-title">
                                                                     <a title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                         Modular Modern </a>
                                                                 </div>
                                                                 <div class="item-price">
                                                                     <div class="price-box">
                                                                <span class="regular-price">
                                                                    <span class="price">
                                                                        <span class="price1">$ 540.00</span>
                                                                    </span>
                                                                </span>
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="action-bot">
                                                             <div class="wrap-addtocart">
                                                                 <button class="btn-cart"
                                                                         title="Add to Cart">
                                                                     <i class="fa fa-shopping-cart"></i>
                                                                     <span>Add to Cart</span>
                                                                 </button>
                                                             </div>
                                                             <div class="actions">
                                                                 <ul class="add-to-links">
                                                                     <li>
                                                                         <a class="link-wishlist"
                                                                            href="#"
                                                                            title="Add to Wishlist">
                                                                             <i class="fa fa-heart"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li>
                                                                         <a class="link-compare"
                                                                            href="#"
                                                                            title="Add to Compare">
                                                                             <i class="fa fa-random"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li class="wrap-quickview" data-id="qv_item_7">
                                                                         <div class="quickview-wrap">
                                                                             <a class="sns-btn-quickview qv_btn"
                                                                                href="#">
                                                                                 <i class="fa fa-eye"></i>
                                                                             </a>
                                                                         </div>
                                                                     </li>
                                                                 </ul>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>

                                             <div class="item col-lg-2d4 col-md-3 col-sm-4 col-xs-6 col-phone-12">
                                                 <div class="item-inner">
                                                     <div class="prd">
                                                         <div class="item-img clearfix">
                                                             <div class="ico-label"></div>
                                                             <a class="product-image have-additional"
                                                                title="Modular Modern"
                                                                href="index3-detail.html">
                                                                <span class="img-main">
                                                               <img src="<?php echo base_url().'assets/images/products/22.jpg';?>" alt="">
                                                                </span>
                                                             </a>
                                                         </div>
                                                         <div class="item-info">
                                                             <div class="info-inner">
                                                                 <div class="item-title">
                                                                     <a title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                         Modular Modern </a>
                                                                 </div>
                                                                 <div class="item-price">
                                                                     <div class="price-box">
                                                                <span class="regular-price">
                                                                    <span class="price">
                                                                        <span class="price1">$ 540.00</span>
                                                                    </span>
                                                                </span>
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="action-bot">
                                                             <div class="wrap-addtocart">
                                                                 <button class="btn-cart"
                                                                         title="Add to Cart">
                                                                     <i class="fa fa-shopping-cart"></i>
                                                                     <span>Add to Cart</span>
                                                                 </button>
                                                             </div>
                                                             <div class="actions">
                                                                 <ul class="add-to-links">
                                                                     <li>
                                                                         <a class="link-wishlist"
                                                                            href="#"
                                                                            title="Add to Wishlist">
                                                                             <i class="fa fa-heart"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li>
                                                                         <a class="link-compare"
                                                                            href="#"
                                                                            title="Add to Compare">
                                                                             <i class="fa fa-random"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li class="wrap-quickview" data-id="qv_item_7">
                                                                         <div class="quickview-wrap">
                                                                             <a class="sns-btn-quickview qv_btn"
                                                                                href="#">
                                                                                 <i class="fa fa-eye"></i>
                                                                             </a>
                                                                         </div>
                                                                     </li>
                                                                 </ul>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>

                                             <div class="item col-lg-2d4 col-md-3 col-sm-4 col-xs-6 col-phone-12">
                                                 <div class="item-inner">
                                                     <div class="prd">
                                                         <div class="item-img clearfix">
                                                             <div class="ico-label"></div>
                                                             <a class="product-image have-additional"
                                                                title="Modular Modern"
                                                                href="index3-detail.html">
                                                                <span class="img-main">
                                                               <img src="<?php echo base_url().'assets/images/products/21.jpg';?>" alt="">
                                                                </span>
                                                             </a>
                                                         </div>
                                                         <div class="item-info">
                                                             <div class="info-inner">
                                                                 <div class="item-title">
                                                                     <a title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                         Modular Modern </a>
                                                                 </div>
                                                                 <div class="item-price">
                                                                     <div class="price-box">
                                                                <span class="regular-price">
                                                                    <span class="price">
                                                                        <span class="price1">$ 540.00</span>
                                                                    </span>
                                                                </span>
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="action-bot">
                                                             <div class="wrap-addtocart">
                                                                 <button class="btn-cart"
                                                                         title="Add to Cart">
                                                                     <i class="fa fa-shopping-cart"></i>
                                                                     <span>Add to Cart</span>
                                                                 </button>
                                                             </div>
                                                             <div class="actions">
                                                                 <ul class="add-to-links">
                                                                     <li>
                                                                         <a class="link-wishlist"
                                                                            href="#"
                                                                            title="Add to Wishlist">
                                                                             <i class="fa fa-heart"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li>
                                                                         <a class="link-compare"
                                                                            href="#"
                                                                            title="Add to Compare">
                                                                             <i class="fa fa-random"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li class="wrap-quickview" data-id="qv_item_7">
                                                                         <div class="quickview-wrap">
                                                                             <a class="sns-btn-quickview qv_btn"
                                                                                href="#">
                                                                                 <i class="fa fa-eye"></i>
                                                                             </a>
                                                                         </div>
                                                                     </li>
                                                                 </ul>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>
                                        </div>
                                    </div>
                                  </div>
                                  <!-- <h3 class="bt-more">
                                    <span>Load more items</span>
                                  </h3> -->
                                </div>   

                                <div class="sns_banner">
                                    <a href="#">
                                        <img src="<?php echo base_url().'assets/images/banner11.png';?>" alt="">
                                    </a>
                                    <!-- <div class="style-title">NEW products</div>
                                    <div class="style-text1">Axel - stool</div>
                                    <div class="style-text2">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries,</div>
                                    <div class="style-button">Buy now</div> -->
                                </div>
                               <!--  <div class="clearfix"></div> -->

                               <div id="sns_producttaps1" class="sns_producttaps_wraps">
                                    <h3 class="precar">PRODUCT TAPS</h3>
                                  <!-- Nav tabs -->
                                  <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Digital Services</a></li>
                                    <!-- <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Wood chair</a></li>
                                    <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Coffee Tables</a></li>
                                    <li role="presentation"><a href="#bedroom" aria-controls="bedroom" role="tab" data-toggle="tab">Bedroom Furniture</a></li> -->
                                  </ul>

                                  <!-- Tab panes -->
                                  <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="home">
                                        <div class="products-grid row style_grid">
                                        <?php foreach($featuredDigital as $row){ ?>
                                            <div class="item col-md-3 col-sm-4 col-xs-6 col-phone-12">
                                                 <div class="item-inner">
                                                     <div class="prd">
                                                         <div class="item-img clearfix">
                                                            <!-- <div class="ico-label">
                                                                <span class="ico-product ico-new">New</span>
                                                                <span class="ico-product ico-sale">Sale</span>
                                                            </div> -->

                                                             <a class="product-image have-additional"
                                                                title="<?php echo $row->name; ?>"
                                                                href="<?php $id = urlencode($row->service_id); echo site_url("ProductDetail/$id"); ?>">
                                                                <span class="img-main">
                                                            <?php if(!empty($row->image)){ ?>
                                                               <img src="<?php echo base_url().'uploads/products/'.$row->image;?>" alt="">
                                                            <?php }else{ ?>
                                                                <img src="<?php echo base_url().'assets/images/default.png'?>" alt="">
                                                            <?php } ?>
                                                                </span>
                                                             </a>
                                                         </div>
                                                         <div class="item-info">
                                                             <div class="info-inner">
                                                                 <div class="item-title">
                                                                     <a title="Modular Modern"
                                                                        href="<?php echo site_url("ProductDetail/$row->service_id"); ?>">
                                                                         <?php echo $row->name; ?> </a>
                                                                 </div>
                                                                 <div class="item-price">
                                                                     <div class="price-box">
                                                                <span class="regular-price">
                                                                    <span class="price">
                                                                        <span class="price1"><?php echo $row->price.'/-'; ?></span>
                                                                        <!-- <span class="price2">600.00</span> -->
                                                                    </span>
                                                                </span>
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="action-bot">
                                                             <div class="wrap-addtocart">
                                                                 <button class="btn-cart"
                                                                         title="Add to Cart">
                                                                     <i class="fa fa-shopping-cart"></i>
                                                                     <span>Add to Cart</span>
                                                                 </button>
                                                             </div>
                                                             <div class="actions">
                                                                 <ul class="add-to-links">
                                                                     <li>
                                                                         <a class="link-wishlist"
                                                                            href="#"
                                                                            title="Add to Wishlist">
                                                                             <i class="fa fa-heart"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li>
                                                                         <a class="link-compare"
                                                                            href="#"
                                                                            title="Add to Compare">
                                                                             <i class="fa fa-random"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li class="wrap-quickview" data-id="qv_item_7">
                                                                         <div class="quickview-wrap">
                                                                             <a class="sns-btn-quickview qv_btn"
                                                                                href="#">
                                                                                 <i class="fa fa-eye"></i>
                                                                             </a>
                                                                         </div>
                                                                     </li>
                                                                 </ul>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>
                                        <?php } ?>
                                        </div>
                                    </div>




                                    <div role="tabpanel" class="tab-pane" id="profile">
                                        <div class="products-grid row style_grid">
                                            <div class="item col-lg-2d4 col-md-3 col-sm-4 col-xs-6 col-phone-12">
                                                 <div class="item-inner">
                                                     <div class="prd">
                                                         <div class="item-img clearfix">
                                                             <div class="ico-label"></div>
                                                             <a class="product-image have-additional"
                                                                title="Modular Modern"
                                                                href="index3-detail.html">
                                                                <span class="img-main">
                                                               <img src="<?php echo base_url().'assets/images/products/30.jpg';?>" alt="">
                                                                </span>
                                                             </a>
                                                         </div>
                                                         <div class="item-info">
                                                             <div class="info-inner">
                                                                 <div class="item-title">
                                                                     <a title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                         Modular Modern </a>
                                                                 </div>
                                                                 <div class="item-price">
                                                                     <div class="price-box">
                                                                <span class="regular-price">
                                                                    <span class="price">
                                                                        <span class="price1">$ 540.00</span>
                                                                        <span class="price2">$ 600.00</span>
                                                                    </span>
                                                                </span>
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="action-bot">
                                                             <div class="wrap-addtocart">
                                                                 <button class="btn-cart"
                                                                         title="Add to Cart">
                                                                     <i class="fa fa-shopping-cart"></i>
                                                                     <span>Add to Cart</span>
                                                                 </button>
                                                             </div>
                                                             <div class="actions">
                                                                 <ul class="add-to-links">
                                                                     <li>
                                                                         <a class="link-wishlist"
                                                                            href="#"
                                                                            title="Add to Wishlist">
                                                                             <i class="fa fa-heart"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li>
                                                                         <a class="link-compare"
                                                                            href="#"
                                                                            title="Add to Compare">
                                                                             <i class="fa fa-random"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li class="wrap-quickview" data-id="qv_item_7">
                                                                         <div class="quickview-wrap">
                                                                             <a class="sns-btn-quickview qv_btn"
                                                                                href="#">
                                                                                 <i class="fa fa-eye"></i>
                                                                             </a>
                                                                         </div>
                                                                     </li>
                                                                 </ul>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>
                                             <div class="item col-lg-2d4 col-md-3 col-sm-4 col-xs-6 col-phone-12">
                                                 <div class="item-inner">
                                                     <div class="prd">
                                                         <div class="item-img clearfix">
                                                             <div class="ico-label"></div>
                                                             <a class="product-image have-additional"
                                                                title="Modular Modern"
                                                                href="index3-detail.html">
                                                                <span class="img-main">
                                                               <img src="<?php echo base_url().'assets/images/products/29.jpg';?>" alt="">
                                                                </span>
                                                             </a>
                                                         </div>
                                                         <div class="item-info">
                                                             <div class="info-inner">
                                                                 <div class="item-title">
                                                                     <a title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                         Modular Modern </a>
                                                                 </div>
                                                                 <div class="item-price">
                                                                     <div class="price-box">
                                                                <span class="regular-price">
                                                                    <span class="price">
                                                                        <span class="price1">$ 540.00</span>
                                                                    </span>
                                                                </span>
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="action-bot">
                                                             <div class="wrap-addtocart">
                                                                 <button class="btn-cart"
                                                                         title="Add to Cart">
                                                                     <i class="fa fa-shopping-cart"></i>
                                                                     <span>Add to Cart</span>
                                                                 </button>
                                                             </div>
                                                             <div class="actions">
                                                                 <ul class="add-to-links">
                                                                     <li>
                                                                         <a class="link-wishlist"
                                                                            href="#"
                                                                            title="Add to Wishlist">
                                                                             <i class="fa fa-heart"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li>
                                                                         <a class="link-compare"
                                                                            href="#"
                                                                            title="Add to Compare">
                                                                             <i class="fa fa-random"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li class="wrap-quickview" data-id="qv_item_7">
                                                                         <div class="quickview-wrap">
                                                                             <a class="sns-btn-quickview qv_btn"
                                                                                href="#">
                                                                                 <i class="fa fa-eye"></i>
                                                                             </a>
                                                                         </div>
                                                                     </li>
                                                                 </ul>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>

                                             <div class="item col-lg-2d4 col-md-3 col-sm-4 col-xs-6 col-phone-12">
                                                 <div class="item-inner">
                                                     <div class="prd">
                                                         <div class="item-img clearfix">
                                                             <div class="ico-label"></div>
                                                             <a class="product-image have-additional"
                                                                title="Modular Modern"
                                                                href="index3-detail.html">
                                                                <span class="img-main">
                                                               <img src="<?php echo base_url().'assets/images/products/28.jpg';?>" alt="">
                                                                </span>
                                                             </a>
                                                         </div>
                                                         <div class="item-info">
                                                             <div class="info-inner">
                                                                 <div class="item-title">
                                                                     <a title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                         Modular Modern </a>
                                                                 </div>
                                                                 <div class="item-price">
                                                                     <div class="price-box">
                                                                <span class="regular-price">
                                                                    <span class="price">
                                                                        <span class="price1">$ 540.00</span>
                                                                    </span>
                                                                </span>
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="action-bot">
                                                             <div class="wrap-addtocart">
                                                                 <button class="btn-cart"
                                                                         title="Add to Cart">
                                                                     <i class="fa fa-shopping-cart"></i>
                                                                     <span>Add to Cart</span>
                                                                 </button>
                                                             </div>
                                                             <div class="actions">
                                                                 <ul class="add-to-links">
                                                                     <li>
                                                                         <a class="link-wishlist"
                                                                            href="#"
                                                                            title="Add to Wishlist">
                                                                             <i class="fa fa-heart"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li>
                                                                         <a class="link-compare"
                                                                            href="#"
                                                                            title="Add to Compare">
                                                                             <i class="fa fa-random"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li class="wrap-quickview" data-id="qv_item_7">
                                                                         <div class="quickview-wrap">
                                                                             <a class="sns-btn-quickview qv_btn"
                                                                                href="#">
                                                                                 <i class="fa fa-eye"></i>
                                                                             </a>
                                                                         </div>
                                                                     </li>
                                                                 </ul>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>

                                             <div class="item col-lg-2d4 col-md-3 col-sm-4 col-xs-6 col-phone-12">
                                                 <div class="item-inner">
                                                     <div class="prd">
                                                         <div class="item-img clearfix">
                                                             <div class="ico-label"></div>
                                                             <a class="product-image have-additional"
                                                                title="Modular Modern"
                                                                href="index3-detail.html">
                                                                <span class="img-main">
                                                               <img src="<?php echo base_url().'assets/images/products/27.jpg';?>" alt="">
                                                                </span>
                                                             </a>
                                                         </div>
                                                         <div class="item-info">
                                                             <div class="info-inner">
                                                                 <div class="item-title">
                                                                     <a title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                         Modular Modern </a>
                                                                 </div>
                                                                 <div class="item-price">
                                                                     <div class="price-box">
                                                                <span class="regular-price">
                                                                    <span class="price">
                                                                        <span class="price1">$ 540.00</span>
                                                                    </span>
                                                                </span>
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="action-bot">
                                                             <div class="wrap-addtocart">
                                                                 <button class="btn-cart"
                                                                         title="Add to Cart">
                                                                     <i class="fa fa-shopping-cart"></i>
                                                                     <span>Add to Cart</span>
                                                                 </button>
                                                             </div>
                                                             <div class="actions">
                                                                 <ul class="add-to-links">
                                                                     <li>
                                                                         <a class="link-wishlist"
                                                                            href="#"
                                                                            title="Add to Wishlist">
                                                                             <i class="fa fa-heart"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li>
                                                                         <a class="link-compare"
                                                                            href="#"
                                                                            title="Add to Compare">
                                                                             <i class="fa fa-random"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li class="wrap-quickview" data-id="qv_item_7">
                                                                         <div class="quickview-wrap">
                                                                             <a class="sns-btn-quickview qv_btn"
                                                                                href="#">
                                                                                 <i class="fa fa-eye"></i>
                                                                             </a>
                                                                         </div>
                                                                     </li>
                                                                 </ul>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>

                                             <div class="item col-lg-2d4 col-md-3 col-sm-4 col-xs-6 col-phone-12">
                                                 <div class="item-inner">
                                                     <div class="prd">
                                                         <div class="item-img clearfix">
                                                             <div class="ico-label"></div>
                                                             <a class="product-image have-additional"
                                                                title="Modular Modern"
                                                                href="index3-detail.html">
                                                                <span class="img-main">
                                                               <img src="<?php echo base_url().'assets/images/products/26.jpg';?>" alt="">
                                                                </span>
                                                             </a>
                                                         </div>
                                                         <div class="item-info">
                                                             <div class="info-inner">
                                                                 <div class="item-title">
                                                                     <a title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                         Modular Modern </a>
                                                                 </div>
                                                                 <div class="item-price">
                                                                     <div class="price-box">
                                                                <span class="regular-price">
                                                                    <span class="price">
                                                                        <span class="price1">$ 540.00</span>
                                                                    </span>
                                                                </span>
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="action-bot">
                                                             <div class="wrap-addtocart">
                                                                 <button class="btn-cart"
                                                                         title="Add to Cart">
                                                                     <i class="fa fa-shopping-cart"></i>
                                                                     <span>Add to Cart</span>
                                                                 </button>
                                                             </div>
                                                             <div class="actions">
                                                                 <ul class="add-to-links">
                                                                     <li>
                                                                         <a class="link-wishlist"
                                                                            href="#"
                                                                            title="Add to Wishlist">
                                                                             <i class="fa fa-heart"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li>
                                                                         <a class="link-compare"
                                                                            href="#"
                                                                            title="Add to Compare">
                                                                             <i class="fa fa-random"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li class="wrap-quickview" data-id="qv_item_7">
                                                                         <div class="quickview-wrap">
                                                                             <a class="sns-btn-quickview qv_btn"
                                                                                href="#">
                                                                                 <i class="fa fa-eye"></i>
                                                                             </a>
                                                                         </div>
                                                                     </li>
                                                                 </ul>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>

                                             <div class="item col-lg-2d4 col-md-3 col-sm-4 col-xs-6 col-phone-12">
                                                 <div class="item-inner">
                                                     <div class="prd">
                                                         <div class="item-img clearfix">
                                                             <div class="ico-label"></div>
                                                             <a class="product-image have-additional"
                                                                title="Modular Modern"
                                                                href="index3-detail.html">
                                                                <span class="img-main">
                                                               <img src="<?php echo base_url().'assets/images/products/25.jpg';?>" alt="">
                                                                </span>
                                                             </a>
                                                         </div>
                                                         <div class="item-info">
                                                             <div class="info-inner">
                                                                 <div class="item-title">
                                                                     <a title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                         Modular Modern </a>
                                                                 </div>
                                                                 <div class="item-price">
                                                                     <div class="price-box">
                                                                <span class="regular-price">
                                                                    <span class="price">
                                                                        <span class="price1">$ 540.00</span>
                                                                        <span class="price2">$ 600.00</span>
                                                                    </span>
                                                                </span>
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="action-bot">
                                                             <div class="wrap-addtocart">
                                                                 <button class="btn-cart"
                                                                         title="Add to Cart">
                                                                     <i class="fa fa-shopping-cart"></i>
                                                                     <span>Add to Cart</span>
                                                                 </button>
                                                             </div>
                                                             <div class="actions">
                                                                 <ul class="add-to-links">
                                                                     <li>
                                                                         <a class="link-wishlist"
                                                                            href="#"
                                                                            title="Add to Wishlist">
                                                                             <i class="fa fa-heart"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li>
                                                                         <a class="link-compare"
                                                                            href="#"
                                                                            title="Add to Compare">
                                                                             <i class="fa fa-random"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li class="wrap-quickview" data-id="qv_item_7">
                                                                         <div class="quickview-wrap">
                                                                             <a class="sns-btn-quickview qv_btn"
                                                                                href="#">
                                                                                 <i class="fa fa-eye"></i>
                                                                             </a>
                                                                         </div>
                                                                     </li>
                                                                 </ul>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>

                                             <div class="item col-lg-2d4 col-md-3 col-sm-4 col-xs-6 col-phone-12">
                                                 <div class="item-inner">
                                                     <div class="prd">
                                                         <div class="item-img clearfix">
                                                             <div class="ico-label"></div>
                                                             <a class="product-image have-additional"
                                                                title="Modular Modern"
                                                                href="index3-detail.html">
                                                                <span class="img-main">
                                                               <img src="<?php echo base_url().'assets/images/products/24.jpg';?>" alt="">
                                                                </span>
                                                             </a>
                                                         </div>
                                                         <div class="item-info">
                                                             <div class="info-inner">
                                                                 <div class="item-title">
                                                                     <a title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                         Modular Modern </a>
                                                                 </div>
                                                                 <div class="item-price">
                                                                     <div class="price-box">
                                                                <span class="regular-price">
                                                                    <span class="price">
                                                                        <span class="price1">$ 540.00</span>
                                                                    </span>
                                                                </span>
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="action-bot">
                                                             <div class="wrap-addtocart">
                                                                 <button class="btn-cart"
                                                                         title="Add to Cart">
                                                                     <i class="fa fa-shopping-cart"></i>
                                                                     <span>Add to Cart</span>
                                                                 </button>
                                                             </div>
                                                             <div class="actions">
                                                                 <ul class="add-to-links">
                                                                     <li>
                                                                         <a class="link-wishlist"
                                                                            href="#"
                                                                            title="Add to Wishlist">
                                                                             <i class="fa fa-heart"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li>
                                                                         <a class="link-compare"
                                                                            href="#"
                                                                            title="Add to Compare">
                                                                             <i class="fa fa-random"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li class="wrap-quickview" data-id="qv_item_7">
                                                                         <div class="quickview-wrap">
                                                                             <a class="sns-btn-quickview qv_btn"
                                                                                href="#">
                                                                                 <i class="fa fa-eye"></i>
                                                                             </a>
                                                                         </div>
                                                                     </li>
                                                                 </ul>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>

                                             <div class="item col-lg-2d4 col-md-3 col-sm-4 col-xs-6 col-phone-12">
                                                 <div class="item-inner">
                                                     <div class="prd">
                                                         <div class="item-img clearfix">
                                                             <div class="ico-label"></div>
                                                             <a class="product-image have-additional"
                                                                title="Modular Modern"
                                                                href="index3-detail.html">
                                                                <span class="img-main">
                                                               <img src="<?php echo base_url().'assets/images/products/23.jpg';?>" alt="">
                                                                </span>
                                                             </a>
                                                         </div>
                                                         <div class="item-info">
                                                             <div class="info-inner">
                                                                 <div class="item-title">
                                                                     <a title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                         Modular Modern </a>
                                                                 </div>
                                                                 <div class="item-price">
                                                                     <div class="price-box">
                                                                <span class="regular-price">
                                                                    <span class="price">
                                                                        <span class="price1">$ 540.00</span>
                                                                    </span>
                                                                </span>
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="action-bot">
                                                             <div class="wrap-addtocart">
                                                                 <button class="btn-cart"
                                                                         title="Add to Cart">
                                                                     <i class="fa fa-shopping-cart"></i>
                                                                     <span>Add to Cart</span>
                                                                 </button>
                                                             </div>
                                                             <div class="actions">
                                                                 <ul class="add-to-links">
                                                                     <li>
                                                                         <a class="link-wishlist"
                                                                            href="#"
                                                                            title="Add to Wishlist">
                                                                             <i class="fa fa-heart"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li>
                                                                         <a class="link-compare"
                                                                            href="#"
                                                                            title="Add to Compare">
                                                                             <i class="fa fa-random"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li class="wrap-quickview" data-id="qv_item_7">
                                                                         <div class="quickview-wrap">
                                                                             <a class="sns-btn-quickview qv_btn"
                                                                                href="#">
                                                                                 <i class="fa fa-eye"></i>
                                                                             </a>
                                                                         </div>
                                                                     </li>
                                                                 </ul>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>

                                             <div class="item col-lg-2d4 col-md-3 col-sm-4 col-xs-6 col-phone-12">
                                                 <div class="item-inner">
                                                     <div class="prd">
                                                         <div class="item-img clearfix">
                                                             <div class="ico-label"></div>
                                                             <a class="product-image have-additional"
                                                                title="Modular Modern"
                                                                href="index3-detail.html">
                                                                <span class="img-main">
                                                               <img src="<?php echo base_url().'assets/images/products/22.jpg';?>" alt="">
                                                                </span>
                                                             </a>
                                                         </div>
                                                         <div class="item-info">
                                                             <div class="info-inner">
                                                                 <div class="item-title">
                                                                     <a title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                         Modular Modern </a>
                                                                 </div>
                                                                 <div class="item-price">
                                                                     <div class="price-box">
                                                                <span class="regular-price">
                                                                    <span class="price">
                                                                        <span class="price1">$ 540.00</span>
                                                                    </span>
                                                                </span>
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="action-bot">
                                                             <div class="wrap-addtocart">
                                                                 <button class="btn-cart"
                                                                         title="Add to Cart">
                                                                     <i class="fa fa-shopping-cart"></i>
                                                                     <span>Add to Cart</span>
                                                                 </button>
                                                             </div>
                                                             <div class="actions">
                                                                 <ul class="add-to-links">
                                                                     <li>
                                                                         <a class="link-wishlist"
                                                                            href="#"
                                                                            title="Add to Wishlist">
                                                                             <i class="fa fa-heart"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li>
                                                                         <a class="link-compare"
                                                                            href="#"
                                                                            title="Add to Compare">
                                                                             <i class="fa fa-random"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li class="wrap-quickview" data-id="qv_item_7">
                                                                         <div class="quickview-wrap">
                                                                             <a class="sns-btn-quickview qv_btn"
                                                                                href="#">
                                                                                 <i class="fa fa-eye"></i>
                                                                             </a>
                                                                         </div>
                                                                     </li>
                                                                 </ul>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>

                                             <div class="item col-lg-2d4 col-md-3 col-sm-4 col-xs-6 col-phone-12">
                                                 <div class="item-inner">
                                                     <div class="prd">
                                                         <div class="item-img clearfix">
                                                             <div class="ico-label"></div>
                                                             <a class="product-image have-additional"
                                                                title="Modular Modern"
                                                                href="index3-detail.html">
                                                                <span class="img-main">
                                                               <img src="<?php echo base_url().'assets/images/products/21.jpg';?>" alt="">
                                                                </span>
                                                             </a>
                                                         </div>
                                                         <div class="item-info">
                                                             <div class="info-inner">
                                                                 <div class="item-title">
                                                                     <a title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                         Modular Modern </a>
                                                                 </div>
                                                                 <div class="item-price">
                                                                     <div class="price-box">
                                                                <span class="regular-price">
                                                                    <span class="price">
                                                                        <span class="price1">$ 540.00</span>
                                                                    </span>
                                                                </span>
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="action-bot">
                                                             <div class="wrap-addtocart">
                                                                 <button class="btn-cart"
                                                                         title="Add to Cart">
                                                                     <i class="fa fa-shopping-cart"></i>
                                                                     <span>Add to Cart</span>
                                                                 </button>
                                                             </div>
                                                             <div class="actions">
                                                                 <ul class="add-to-links">
                                                                     <li>
                                                                         <a class="link-wishlist"
                                                                            href="#"
                                                                            title="Add to Wishlist">
                                                                             <i class="fa fa-heart"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li>
                                                                         <a class="link-compare"
                                                                            href="#"
                                                                            title="Add to Compare">
                                                                             <i class="fa fa-random"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li class="wrap-quickview" data-id="qv_item_7">
                                                                         <div class="quickview-wrap">
                                                                             <a class="sns-btn-quickview qv_btn"
                                                                                href="#">
                                                                                 <i class="fa fa-eye"></i>
                                                                             </a>
                                                                         </div>
                                                                     </li>
                                                                 </ul>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>
                                        </div>
                                    </div>


                                    <div role="tabpanel" class="tab-pane" id="messages">
                                        <div class="products-grid row style_grid">
                                            <div class="item col-lg-2d4 col-md-3 col-sm-4 col-xs-6 col-phone-12">
                                                 <div class="item-inner">
                                                     <div class="prd">
                                                         <div class="item-img clearfix">
                                                             <div class="ico-label"></div>
                                                             <a class="product-image have-additional"
                                                                title="Modular Modern"
                                                                href="index3-detail.html">
                                                                <span class="img-main">
                                                               <img src="<?php echo base_url().'assets/images/products/3.jpg';?>" alt="">
                                                                </span>
                                                             </a>
                                                         </div>
                                                         <div class="item-info">
                                                             <div class="info-inner">
                                                                 <div class="item-title">
                                                                     <a title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                         Modular Modern </a>
                                                                 </div>
                                                                 <div class="item-price">
                                                                     <div class="price-box">
                                                                <span class="regular-price">
                                                                    <span class="price">
                                                                        <span class="price1">$ 540.00</span>
                                                                        <span class="price2">$ 600.00</span>
                                                                    </span>
                                                                </span>
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="action-bot">
                                                             <div class="wrap-addtocart">
                                                                 <button class="btn-cart"
                                                                         title="Add to Cart">
                                                                     <i class="fa fa-shopping-cart"></i>
                                                                     <span>Add to Cart</span>
                                                                 </button>
                                                             </div>
                                                             <div class="actions">
                                                                 <ul class="add-to-links">
                                                                     <li>
                                                                         <a class="link-wishlist"
                                                                            href="#"
                                                                            title="Add to Wishlist">
                                                                             <i class="fa fa-heart"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li>
                                                                         <a class="link-compare"
                                                                            href="#"
                                                                            title="Add to Compare">
                                                                             <i class="fa fa-random"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li class="wrap-quickview" data-id="qv_item_7">
                                                                         <div class="quickview-wrap">
                                                                             <a class="sns-btn-quickview qv_btn"
                                                                                href="#">
                                                                                 <i class="fa fa-eye"></i>
                                                                             </a>
                                                                         </div>
                                                                     </li>
                                                                 </ul>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>
                                             <div class="item col-lg-2d4 col-md-3 col-sm-4 col-xs-6 col-phone-12">
                                                 <div class="item-inner">
                                                     <div class="prd">
                                                         <div class="item-img clearfix">
                                                             <div class="ico-label"></div>
                                                             <a class="product-image have-additional"
                                                                title="Modular Modern"
                                                                href="index3-detail.html">
                                                                <span class="img-main">
                                                               <img src="<?php echo base_url().'assets/images/products/5.jpg';?>" alt="">
                                                                </span>
                                                             </a>
                                                         </div>
                                                         <div class="item-info">
                                                             <div class="info-inner">
                                                                 <div class="item-title">
                                                                     <a title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                         Modular Modern </a>
                                                                 </div>
                                                                 <div class="item-price">
                                                                     <div class="price-box">
                                                                <span class="regular-price">
                                                                    <span class="price">
                                                                        <span class="price1">$ 540.00</span>
                                                                    </span>
                                                                </span>
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="action-bot">
                                                             <div class="wrap-addtocart">
                                                                 <button class="btn-cart"
                                                                         title="Add to Cart">
                                                                     <i class="fa fa-shopping-cart"></i>
                                                                     <span>Add to Cart</span>
                                                                 </button>
                                                             </div>
                                                             <div class="actions">
                                                                 <ul class="add-to-links">
                                                                     <li>
                                                                         <a class="link-wishlist"
                                                                            href="#"
                                                                            title="Add to Wishlist">
                                                                             <i class="fa fa-heart"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li>
                                                                         <a class="link-compare"
                                                                            href="#"
                                                                            title="Add to Compare">
                                                                             <i class="fa fa-random"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li class="wrap-quickview" data-id="qv_item_7">
                                                                         <div class="quickview-wrap">
                                                                             <a class="sns-btn-quickview qv_btn"
                                                                                href="#">
                                                                                 <i class="fa fa-eye"></i>
                                                                             </a>
                                                                         </div>
                                                                     </li>
                                                                 </ul>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>

                                             <div class="item col-lg-2d4 col-md-3 col-sm-4 col-xs-6 col-phone-12">
                                                 <div class="item-inner">
                                                     <div class="prd">
                                                         <div class="item-img clearfix">
                                                             <div class="ico-label"></div>
                                                             <a class="product-image have-additional"
                                                                title="Modular Modern"
                                                                href="index3-detail.html">
                                                                <span class="img-main">
                                                               <img src="<?php echo base_url().'assets/images/products/7.jpg';?>" alt="">
                                                                </span>
                                                             </a>
                                                         </div>
                                                         <div class="item-info">
                                                             <div class="info-inner">
                                                                 <div class="item-title">
                                                                     <a title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                         Modular Modern </a>
                                                                 </div>
                                                                 <div class="item-price">
                                                                     <div class="price-box">
                                                                <span class="regular-price">
                                                                    <span class="price">
                                                                        <span class="price1">$ 540.00</span>
                                                                    </span>
                                                                </span>
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="action-bot">
                                                             <div class="wrap-addtocart">
                                                                 <button class="btn-cart"
                                                                         title="Add to Cart">
                                                                     <i class="fa fa-shopping-cart"></i>
                                                                     <span>Add to Cart</span>
                                                                 </button>
                                                             </div>
                                                             <div class="actions">
                                                                 <ul class="add-to-links">
                                                                     <li>
                                                                         <a class="link-wishlist"
                                                                            href="#"
                                                                            title="Add to Wishlist">
                                                                             <i class="fa fa-heart"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li>
                                                                         <a class="link-compare"
                                                                            href="#"
                                                                            title="Add to Compare">
                                                                             <i class="fa fa-random"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li class="wrap-quickview" data-id="qv_item_7">
                                                                         <div class="quickview-wrap">
                                                                             <a class="sns-btn-quickview qv_btn"
                                                                                href="#">
                                                                                 <i class="fa fa-eye"></i>
                                                                             </a>
                                                                         </div>
                                                                     </li>
                                                                 </ul>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>

                                             <div class="item col-lg-2d4 col-md-3 col-sm-4 col-xs-6 col-phone-12">
                                                 <div class="item-inner">
                                                     <div class="prd">
                                                         <div class="item-img clearfix">
                                                             <div class="ico-label"></div>
                                                             <a class="product-image have-additional"
                                                                title="Modular Modern"
                                                                href="index3-detail.html">
                                                                <span class="img-main">
                                                               <img src="<?php echo base_url().'assets/images/products/9.jpg';?>" alt="">
                                                                </span>
                                                             </a>
                                                         </div>
                                                         <div class="item-info">
                                                             <div class="info-inner">
                                                                 <div class="item-title">
                                                                     <a title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                         Modular Modern </a>
                                                                 </div>
                                                                 <div class="item-price">
                                                                     <div class="price-box">
                                                                <span class="regular-price">
                                                                    <span class="price">
                                                                        <span class="price1">$ 540.00</span>
                                                                    </span>
                                                                </span>
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="action-bot">
                                                             <div class="wrap-addtocart">
                                                                 <button class="btn-cart"
                                                                         title="Add to Cart">
                                                                     <i class="fa fa-shopping-cart"></i>
                                                                     <span>Add to Cart</span>
                                                                 </button>
                                                             </div>
                                                             <div class="actions">
                                                                 <ul class="add-to-links">
                                                                     <li>
                                                                         <a class="link-wishlist"
                                                                            href="#"
                                                                            title="Add to Wishlist">
                                                                             <i class="fa fa-heart"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li>
                                                                         <a class="link-compare"
                                                                            href="#"
                                                                            title="Add to Compare">
                                                                             <i class="fa fa-random"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li class="wrap-quickview" data-id="qv_item_7">
                                                                         <div class="quickview-wrap">
                                                                             <a class="sns-btn-quickview qv_btn"
                                                                                href="#">
                                                                                 <i class="fa fa-eye"></i>
                                                                             </a>
                                                                         </div>
                                                                     </li>
                                                                 </ul>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>

                                             <div class="item col-lg-2d4 col-md-3 col-sm-4 col-xs-6 col-phone-12">
                                                 <div class="item-inner">
                                                     <div class="prd">
                                                         <div class="item-img clearfix">
                                                             <div class="ico-label"></div>
                                                             <a class="product-image have-additional"
                                                                title="Modular Modern"
                                                                href="index3-detail.html">
                                                                <span class="img-main">
                                                               <img src="<?php echo base_url().'assets/images/products/11.jpg';?>" alt="">
                                                                </span>
                                                             </a>
                                                         </div>
                                                         <div class="item-info">
                                                             <div class="info-inner">
                                                                 <div class="item-title">
                                                                     <a title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                         Modular Modern </a>
                                                                 </div>
                                                                 <div class="item-price">
                                                                     <div class="price-box">
                                                                <span class="regular-price">
                                                                    <span class="price">
                                                                        <span class="price1">$ 540.00</span>
                                                                    </span>
                                                                </span>
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="action-bot">
                                                             <div class="wrap-addtocart">
                                                                 <button class="btn-cart"
                                                                         title="Add to Cart">
                                                                     <i class="fa fa-shopping-cart"></i>
                                                                     <span>Add to Cart</span>
                                                                 </button>
                                                             </div>
                                                             <div class="actions">
                                                                 <ul class="add-to-links">
                                                                     <li>
                                                                         <a class="link-wishlist"
                                                                            href="#"
                                                                            title="Add to Wishlist">
                                                                             <i class="fa fa-heart"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li>
                                                                         <a class="link-compare"
                                                                            href="#"
                                                                            title="Add to Compare">
                                                                             <i class="fa fa-random"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li class="wrap-quickview" data-id="qv_item_7">
                                                                         <div class="quickview-wrap">
                                                                             <a class="sns-btn-quickview qv_btn"
                                                                                href="#">
                                                                                 <i class="fa fa-eye"></i>
                                                                             </a>
                                                                         </div>
                                                                     </li>
                                                                 </ul>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>

                                             <div class="item col-lg-2d4 col-md-3 col-sm-4 col-xs-6 col-phone-12">
                                                 <div class="item-inner">
                                                     <div class="prd">
                                                         <div class="item-img clearfix">
                                                             <div class="ico-label"></div>
                                                             <a class="product-image have-additional"
                                                                title="Modular Modern"
                                                                href="index3-detail.html">
                                                                <span class="img-main">
                                                               <img src="<?php echo base_url().'assets/images/products/13.jpg';?>" alt="">
                                                                </span>
                                                             </a>
                                                         </div>
                                                         <div class="item-info">
                                                             <div class="info-inner">
                                                                 <div class="item-title">
                                                                     <a title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                         Modular Modern </a>
                                                                 </div>
                                                                 <div class="item-price">
                                                                     <div class="price-box">
                                                                <span class="regular-price">
                                                                    <span class="price">
                                                                        <span class="price1">$ 540.00</span>
                                                                        <span class="price2">$ 600.00</span>
                                                                    </span>
                                                                </span>
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="action-bot">
                                                             <div class="wrap-addtocart">
                                                                 <button class="btn-cart"
                                                                         title="Add to Cart">
                                                                     <i class="fa fa-shopping-cart"></i>
                                                                     <span>Add to Cart</span>
                                                                 </button>
                                                             </div>
                                                             <div class="actions">
                                                                 <ul class="add-to-links">
                                                                     <li>
                                                                         <a class="link-wishlist"
                                                                            href="#"
                                                                            title="Add to Wishlist">
                                                                             <i class="fa fa-heart"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li>
                                                                         <a class="link-compare"
                                                                            href="#"
                                                                            title="Add to Compare">
                                                                             <i class="fa fa-random"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li class="wrap-quickview" data-id="qv_item_7">
                                                                         <div class="quickview-wrap">
                                                                             <a class="sns-btn-quickview qv_btn"
                                                                                href="#">
                                                                                 <i class="fa fa-eye"></i>
                                                                             </a>
                                                                         </div>
                                                                     </li>
                                                                 </ul>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>

                                             <div class="item col-lg-2d4 col-md-3 col-sm-4 col-xs-6 col-phone-12">
                                                 <div class="item-inner">
                                                     <div class="prd">
                                                         <div class="item-img clearfix">
                                                             <div class="ico-label"></div>
                                                             <a class="product-image have-additional"
                                                                title="Modular Modern"
                                                                href="index3-detail.html">
                                                                <span class="img-main">
                                                               <img src="<?php echo base_url().'assets/images/products/15.jpg';?>" alt="">
                                                                </span>
                                                             </a>
                                                         </div>
                                                         <div class="item-info">
                                                             <div class="info-inner">
                                                                 <div class="item-title">
                                                                     <a title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                         Modular Modern </a>
                                                                 </div>
                                                                 <div class="item-price">
                                                                     <div class="price-box">
                                                                <span class="regular-price">
                                                                    <span class="price">
                                                                        <span class="price1">$ 540.00</span>
                                                                    </span>
                                                                </span>
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="action-bot">
                                                             <div class="wrap-addtocart">
                                                                 <button class="btn-cart"
                                                                         title="Add to Cart">
                                                                     <i class="fa fa-shopping-cart"></i>
                                                                     <span>Add to Cart</span>
                                                                 </button>
                                                             </div>
                                                             <div class="actions">
                                                                 <ul class="add-to-links">
                                                                     <li>
                                                                         <a class="link-wishlist"
                                                                            href="#"
                                                                            title="Add to Wishlist">
                                                                             <i class="fa fa-heart"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li>
                                                                         <a class="link-compare"
                                                                            href="#"
                                                                            title="Add to Compare">
                                                                             <i class="fa fa-random"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li class="wrap-quickview" data-id="qv_item_7">
                                                                         <div class="quickview-wrap">
                                                                             <a class="sns-btn-quickview qv_btn"
                                                                                href="#">
                                                                                 <i class="fa fa-eye"></i>
                                                                             </a>
                                                                         </div>
                                                                     </li>
                                                                 </ul>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>

                                             <div class="item col-lg-2d4 col-md-3 col-sm-4 col-xs-6 col-phone-12">
                                                 <div class="item-inner">
                                                     <div class="prd">
                                                         <div class="item-img clearfix">
                                                             <div class="ico-label"></div>
                                                             <a class="product-image have-additional"
                                                                title="Modular Modern"
                                                                href="index3-detail.html">
                                                                <span class="img-main">
                                                               <img src="<?php echo base_url().'assets/images/products/17.jpg';?>" alt="">
                                                                </span>
                                                             </a>
                                                         </div>
                                                         <div class="item-info">
                                                             <div class="info-inner">
                                                                 <div class="item-title">
                                                                     <a title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                         Modular Modern </a>
                                                                 </div>
                                                                 <div class="item-price">
                                                                     <div class="price-box">
                                                                <span class="regular-price">
                                                                    <span class="price">
                                                                        <span class="price1">$ 540.00</span>
                                                                    </span>
                                                                </span>
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="action-bot">
                                                             <div class="wrap-addtocart">
                                                                 <button class="btn-cart"
                                                                         title="Add to Cart">
                                                                     <i class="fa fa-shopping-cart"></i>
                                                                     <span>Add to Cart</span>
                                                                 </button>
                                                             </div>
                                                             <div class="actions">
                                                                 <ul class="add-to-links">
                                                                     <li>
                                                                         <a class="link-wishlist"
                                                                            href="#"
                                                                            title="Add to Wishlist">
                                                                             <i class="fa fa-heart"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li>
                                                                         <a class="link-compare"
                                                                            href="#"
                                                                            title="Add to Compare">
                                                                             <i class="fa fa-random"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li class="wrap-quickview" data-id="qv_item_7">
                                                                         <div class="quickview-wrap">
                                                                             <a class="sns-btn-quickview qv_btn"
                                                                                href="#">
                                                                                 <i class="fa fa-eye"></i>
                                                                             </a>
                                                                         </div>
                                                                     </li>
                                                                 </ul>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>

                                             <div class="item col-lg-2d4 col-md-3 col-sm-4 col-xs-6 col-phone-12">
                                                 <div class="item-inner">
                                                     <div class="prd">
                                                         <div class="item-img clearfix">
                                                             <div class="ico-label"></div>
                                                             <a class="product-image have-additional"
                                                                title="Modular Modern"
                                                                href="index3-detail.html">
                                                                <span class="img-main">
                                                               <img src="<?php echo base_url().'assets/images/products/19.jpg';?>" alt="">
                                                                </span>
                                                             </a>
                                                         </div>
                                                         <div class="item-info">
                                                             <div class="info-inner">
                                                                 <div class="item-title">
                                                                     <a title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                         Modular Modern </a>
                                                                 </div>
                                                                 <div class="item-price">
                                                                     <div class="price-box">
                                                                <span class="regular-price">
                                                                    <span class="price">
                                                                        <span class="price1">$ 540.00</span>
                                                                    </span>
                                                                </span>
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="action-bot">
                                                             <div class="wrap-addtocart">
                                                                 <button class="btn-cart"
                                                                         title="Add to Cart">
                                                                     <i class="fa fa-shopping-cart"></i>
                                                                     <span>Add to Cart</span>
                                                                 </button>
                                                             </div>
                                                             <div class="actions">
                                                                 <ul class="add-to-links">
                                                                     <li>
                                                                         <a class="link-wishlist"
                                                                            href="#"
                                                                            title="Add to Wishlist">
                                                                             <i class="fa fa-heart"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li>
                                                                         <a class="link-compare"
                                                                            href="#"
                                                                            title="Add to Compare">
                                                                             <i class="fa fa-random"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li class="wrap-quickview" data-id="qv_item_7">
                                                                         <div class="quickview-wrap">
                                                                             <a class="sns-btn-quickview qv_btn"
                                                                                href="#">
                                                                                 <i class="fa fa-eye"></i>
                                                                             </a>
                                                                         </div>
                                                                     </li>
                                                                 </ul>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>

                                             <div class="item col-lg-2d4 col-md-3 col-sm-4 col-xs-6 col-phone-12">
                                                 <div class="item-inner">
                                                     <div class="prd">
                                                         <div class="item-img clearfix">
                                                             <div class="ico-label"></div>
                                                             <a class="product-image have-additional"
                                                                title="Modular Modern"
                                                                href="index3-detail.html">
                                                                <span class="img-main">
                                                               <img src="<?php echo base_url().'assets/images/products/21.jpg';?>" alt="">
                                                                </span>
                                                             </a>
                                                         </div>
                                                         <div class="item-info">
                                                             <div class="info-inner">
                                                                 <div class="item-title">
                                                                     <a title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                         Modular Modern </a>
                                                                 </div>
                                                                 <div class="item-price">
                                                                     <div class="price-box">
                                                                <span class="regular-price">
                                                                    <span class="price">
                                                                        <span class="price1">$ 540.00</span>
                                                                    </span>
                                                                </span>
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="action-bot">
                                                             <div class="wrap-addtocart">
                                                                 <button class="btn-cart"
                                                                         title="Add to Cart">
                                                                     <i class="fa fa-shopping-cart"></i>
                                                                     <span>Add to Cart</span>
                                                                 </button>
                                                             </div>
                                                             <div class="actions">
                                                                 <ul class="add-to-links">
                                                                     <li>
                                                                         <a class="link-wishlist"
                                                                            href="#"
                                                                            title="Add to Wishlist">
                                                                             <i class="fa fa-heart"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li>
                                                                         <a class="link-compare"
                                                                            href="#"
                                                                            title="Add to Compare">
                                                                             <i class="fa fa-random"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li class="wrap-quickview" data-id="qv_item_7">
                                                                         <div class="quickview-wrap">
                                                                             <a class="sns-btn-quickview qv_btn"
                                                                                href="#">
                                                                                 <i class="fa fa-eye"></i>
                                                                             </a>
                                                                         </div>
                                                                     </li>
                                                                 </ul>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>
                                        </div>
                                    </div>

                                    <div role="tabpanel" class="tab-pane" id="bedroom">
                                        <div class="products-grid row style_grid">
                                            <div class="item col-lg-2d4 col-md-3 col-sm-4 col-xs-6 col-phone-12">
                                                 <div class="item-inner">
                                                     <div class="prd">
                                                         <div class="item-img clearfix">
                                                             <div class="ico-label"></div>
                                                             <a class="product-image have-additional"
                                                                title="Modular Modern"
                                                                href="index3-detail.html">
                                                                <span class="img-main">
                                                               <img src="<?php echo base_url().'assets/images/products/30.jpg';?>" alt="">
                                                                </span>
                                                             </a>
                                                         </div>
                                                         <div class="item-info">
                                                             <div class="info-inner">
                                                                 <div class="item-title">
                                                                     <a title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                         Modular Modern </a>
                                                                 </div>
                                                                 <div class="item-price">
                                                                     <div class="price-box">
                                                                <span class="regular-price">
                                                                    <span class="price">
                                                                        <span class="price1">$ 540.00</span>
                                                                        <span class="price2">$ 600.00</span>
                                                                    </span>
                                                                </span>
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="action-bot">
                                                             <div class="wrap-addtocart">
                                                                 <button class="btn-cart"
                                                                         title="Add to Cart">
                                                                     <i class="fa fa-shopping-cart"></i>
                                                                     <span>Add to Cart</span>
                                                                 </button>
                                                             </div>
                                                             <div class="actions">
                                                                 <ul class="add-to-links">
                                                                     <li>
                                                                         <a class="link-wishlist"
                                                                            href="#"
                                                                            title="Add to Wishlist">
                                                                             <i class="fa fa-heart"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li>
                                                                         <a class="link-compare"
                                                                            href="#"
                                                                            title="Add to Compare">
                                                                             <i class="fa fa-random"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li class="wrap-quickview" data-id="qv_item_7">
                                                                         <div class="quickview-wrap">
                                                                             <a class="sns-btn-quickview qv_btn"
                                                                                href="#">
                                                                                 <i class="fa fa-eye"></i>
                                                                             </a>
                                                                         </div>
                                                                     </li>
                                                                 </ul>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>
                                             <div class="item col-lg-2d4 col-md-3 col-sm-4 col-xs-6 col-phone-12">
                                                 <div class="item-inner">
                                                     <div class="prd">
                                                         <div class="item-img clearfix">
                                                             <div class="ico-label"></div>
                                                             <a class="product-image have-additional"
                                                                title="Modular Modern"
                                                                href="index3-detail.html">
                                                                <span class="img-main">
                                                               <img src="<?php echo base_url().'assets/images/products/29.jpg';?>" alt="">
                                                                </span>
                                                             </a>
                                                         </div>
                                                         <div class="item-info">
                                                             <div class="info-inner">
                                                                 <div class="item-title">
                                                                     <a title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                         Modular Modern </a>
                                                                 </div>
                                                                 <div class="item-price">
                                                                     <div class="price-box">
                                                                <span class="regular-price">
                                                                    <span class="price">
                                                                        <span class="price1">$ 540.00</span>
                                                                    </span>
                                                                </span>
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="action-bot">
                                                             <div class="wrap-addtocart">
                                                                 <button class="btn-cart"
                                                                         title="Add to Cart">
                                                                     <i class="fa fa-shopping-cart"></i>
                                                                     <span>Add to Cart</span>
                                                                 </button>
                                                             </div>
                                                             <div class="actions">
                                                                 <ul class="add-to-links">
                                                                     <li>
                                                                         <a class="link-wishlist"
                                                                            href="#"
                                                                            title="Add to Wishlist">
                                                                             <i class="fa fa-heart"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li>
                                                                         <a class="link-compare"
                                                                            href="#"
                                                                            title="Add to Compare">
                                                                             <i class="fa fa-random"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li class="wrap-quickview" data-id="qv_item_7">
                                                                         <div class="quickview-wrap">
                                                                             <a class="sns-btn-quickview qv_btn"
                                                                                href="#">
                                                                                 <i class="fa fa-eye"></i>
                                                                             </a>
                                                                         </div>
                                                                     </li>
                                                                 </ul>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>

                                             <div class="item col-lg-2d4 col-md-3 col-sm-4 col-xs-6 col-phone-12">
                                                 <div class="item-inner">
                                                     <div class="prd">
                                                         <div class="item-img clearfix">
                                                             <div class="ico-label"></div>
                                                             <a class="product-image have-additional"
                                                                title="Modular Modern"
                                                                href="index3-detail.html">
                                                                <span class="img-main">
                                                               <img src="<?php echo base_url().'assets/images/products/28.jpg';?>" alt="">
                                                                </span>
                                                             </a>
                                                         </div>
                                                         <div class="item-info">
                                                             <div class="info-inner">
                                                                 <div class="item-title">
                                                                     <a title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                         Modular Modern </a>
                                                                 </div>
                                                                 <div class="item-price">
                                                                     <div class="price-box">
                                                                <span class="regular-price">
                                                                    <span class="price">
                                                                        <span class="price1">$ 540.00</span>
                                                                    </span>
                                                                </span>
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="action-bot">
                                                             <div class="wrap-addtocart">
                                                                 <button class="btn-cart"
                                                                         title="Add to Cart">
                                                                     <i class="fa fa-shopping-cart"></i>
                                                                     <span>Add to Cart</span>
                                                                 </button>
                                                             </div>
                                                             <div class="actions">
                                                                 <ul class="add-to-links">
                                                                     <li>
                                                                         <a class="link-wishlist"
                                                                            href="#"
                                                                            title="Add to Wishlist">
                                                                             <i class="fa fa-heart"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li>
                                                                         <a class="link-compare"
                                                                            href="#"
                                                                            title="Add to Compare">
                                                                             <i class="fa fa-random"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li class="wrap-quickview" data-id="qv_item_7">
                                                                         <div class="quickview-wrap">
                                                                             <a class="sns-btn-quickview qv_btn"
                                                                                href="#">
                                                                                 <i class="fa fa-eye"></i>
                                                                             </a>
                                                                         </div>
                                                                     </li>
                                                                 </ul>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>

                                             <div class="item col-lg-2d4 col-md-3 col-sm-4 col-xs-6 col-phone-12">
                                                 <div class="item-inner">
                                                     <div class="prd">
                                                         <div class="item-img clearfix">
                                                             <div class="ico-label"></div>
                                                             <a class="product-image have-additional"
                                                                title="Modular Modern"
                                                                href="index3-detail.html">
                                                                <span class="img-main">
                                                               <img src="<?php echo base_url().'assets/images/products/27.jpg';?>" alt="">
                                                                </span>
                                                             </a>
                                                         </div>
                                                         <div class="item-info">
                                                             <div class="info-inner">
                                                                 <div class="item-title">
                                                                     <a title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                         Modular Modern </a>
                                                                 </div>
                                                                 <div class="item-price">
                                                                     <div class="price-box">
                                                                <span class="regular-price">
                                                                    <span class="price">
                                                                        <span class="price1">$ 540.00</span>
                                                                    </span>
                                                                </span>
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="action-bot">
                                                             <div class="wrap-addtocart">
                                                                 <button class="btn-cart"
                                                                         title="Add to Cart">
                                                                     <i class="fa fa-shopping-cart"></i>
                                                                     <span>Add to Cart</span>
                                                                 </button>
                                                             </div>
                                                             <div class="actions">
                                                                 <ul class="add-to-links">
                                                                     <li>
                                                                         <a class="link-wishlist"
                                                                            href="#"
                                                                            title="Add to Wishlist">
                                                                             <i class="fa fa-heart"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li>
                                                                         <a class="link-compare"
                                                                            href="#"
                                                                            title="Add to Compare">
                                                                             <i class="fa fa-random"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li class="wrap-quickview" data-id="qv_item_7">
                                                                         <div class="quickview-wrap">
                                                                             <a class="sns-btn-quickview qv_btn"
                                                                                href="#">
                                                                                 <i class="fa fa-eye"></i>
                                                                             </a>
                                                                         </div>
                                                                     </li>
                                                                 </ul>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>

                                             <div class="item col-lg-2d4 col-md-3 col-sm-4 col-xs-6 col-phone-12">
                                                 <div class="item-inner">
                                                     <div class="prd">
                                                         <div class="item-img clearfix">
                                                             <div class="ico-label"></div>
                                                             <a class="product-image have-additional"
                                                                title="Modular Modern"
                                                                href="index3-detail.html">
                                                                <span class="img-main">
                                                               <img src="<?php echo base_url().'assets/images/products/26.jpg';?>" alt="">
                                                                </span>
                                                             </a>
                                                         </div>
                                                         <div class="item-info">
                                                             <div class="info-inner">
                                                                 <div class="item-title">
                                                                     <a title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                         Modular Modern </a>
                                                                 </div>
                                                                 <div class="item-price">
                                                                     <div class="price-box">
                                                                <span class="regular-price">
                                                                    <span class="price">
                                                                        <span class="price1">$ 540.00</span>
                                                                    </span>
                                                                </span>
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="action-bot">
                                                             <div class="wrap-addtocart">
                                                                 <button class="btn-cart"
                                                                         title="Add to Cart">
                                                                     <i class="fa fa-shopping-cart"></i>
                                                                     <span>Add to Cart</span>
                                                                 </button>
                                                             </div>
                                                             <div class="actions">
                                                                 <ul class="add-to-links">
                                                                     <li>
                                                                         <a class="link-wishlist"
                                                                            href="#"
                                                                            title="Add to Wishlist">
                                                                             <i class="fa fa-heart"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li>
                                                                         <a class="link-compare"
                                                                            href="#"
                                                                            title="Add to Compare">
                                                                             <i class="fa fa-random"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li class="wrap-quickview" data-id="qv_item_7">
                                                                         <div class="quickview-wrap">
                                                                             <a class="sns-btn-quickview qv_btn"
                                                                                href="#">
                                                                                 <i class="fa fa-eye"></i>
                                                                             </a>
                                                                         </div>
                                                                     </li>
                                                                 </ul>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>

                                             <div class="item col-lg-2d4 col-md-3 col-sm-4 col-xs-6 col-phone-12">
                                                 <div class="item-inner">
                                                     <div class="prd">
                                                         <div class="item-img clearfix">
                                                             <div class="ico-label"></div>
                                                             <a class="product-image have-additional"
                                                                title="Modular Modern"
                                                                href="index3-detail.html">
                                                                <span class="img-main">
                                                               <img src="<?php echo base_url().'assets/images/products/25.jpg';?>" alt="">
                                                                </span>
                                                             </a>
                                                         </div>
                                                         <div class="item-info">
                                                             <div class="info-inner">
                                                                 <div class="item-title">
                                                                     <a title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                         Modular Modern </a>
                                                                 </div>
                                                                 <div class="item-price">
                                                                     <div class="price-box">
                                                                <span class="regular-price">
                                                                    <span class="price">
                                                                        <span class="price1">$ 540.00</span>
                                                                        <span class="price2">$ 600.00</span>
                                                                    </span>
                                                                </span>
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="action-bot">
                                                             <div class="wrap-addtocart">
                                                                 <button class="btn-cart"
                                                                         title="Add to Cart">
                                                                     <i class="fa fa-shopping-cart"></i>
                                                                     <span>Add to Cart</span>
                                                                 </button>
                                                             </div>
                                                             <div class="actions">
                                                                 <ul class="add-to-links">
                                                                     <li>
                                                                         <a class="link-wishlist"
                                                                            href="#"
                                                                            title="Add to Wishlist">
                                                                             <i class="fa fa-heart"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li>
                                                                         <a class="link-compare"
                                                                            href="#"
                                                                            title="Add to Compare">
                                                                             <i class="fa fa-random"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li class="wrap-quickview" data-id="qv_item_7">
                                                                         <div class="quickview-wrap">
                                                                             <a class="sns-btn-quickview qv_btn"
                                                                                href="#">
                                                                                 <i class="fa fa-eye"></i>
                                                                             </a>
                                                                         </div>
                                                                     </li>
                                                                 </ul>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>

                                             <div class="item col-lg-2d4 col-md-3 col-sm-4 col-xs-6 col-phone-12">
                                                 <div class="item-inner">
                                                     <div class="prd">
                                                         <div class="item-img clearfix">
                                                             <div class="ico-label"></div>
                                                             <a class="product-image have-additional"
                                                                title="Modular Modern"
                                                                href="index3-detail.html">
                                                                <span class="img-main">
                                                               <img src="<?php echo base_url().'assets/images/products/24.jpg';?>" alt="">
                                                                </span>
                                                             </a>
                                                         </div>
                                                         <div class="item-info">
                                                             <div class="info-inner">
                                                                 <div class="item-title">
                                                                     <a title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                         Modular Modern </a>
                                                                 </div>
                                                                 <div class="item-price">
                                                                     <div class="price-box">
                                                                <span class="regular-price">
                                                                    <span class="price">
                                                                        <span class="price1">$ 540.00</span>
                                                                    </span>
                                                                </span>
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="action-bot">
                                                             <div class="wrap-addtocart">
                                                                 <button class="btn-cart"
                                                                         title="Add to Cart">
                                                                     <i class="fa fa-shopping-cart"></i>
                                                                     <span>Add to Cart</span>
                                                                 </button>
                                                             </div>
                                                             <div class="actions">
                                                                 <ul class="add-to-links">
                                                                     <li>
                                                                         <a class="link-wishlist"
                                                                            href="#"
                                                                            title="Add to Wishlist">
                                                                             <i class="fa fa-heart"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li>
                                                                         <a class="link-compare"
                                                                            href="#"
                                                                            title="Add to Compare">
                                                                             <i class="fa fa-random"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li class="wrap-quickview" data-id="qv_item_7">
                                                                         <div class="quickview-wrap">
                                                                             <a class="sns-btn-quickview qv_btn"
                                                                                href="#">
                                                                                 <i class="fa fa-eye"></i>
                                                                             </a>
                                                                         </div>
                                                                     </li>
                                                                 </ul>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>

                                             <div class="item col-lg-2d4 col-md-3 col-sm-4 col-xs-6 col-phone-12">
                                                 <div class="item-inner">
                                                     <div class="prd">
                                                         <div class="item-img clearfix">
                                                             <div class="ico-label"></div>
                                                             <a class="product-image have-additional"
                                                                title="Modular Modern"
                                                                href="index3-detail.html">
                                                                <span class="img-main">
                                                               <img src="<?php echo base_url().'assets/images/products/23.jpg';?>" alt="">
                                                                </span>
                                                             </a>
                                                         </div>
                                                         <div class="item-info">
                                                             <div class="info-inner">
                                                                 <div class="item-title">
                                                                     <a title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                         Modular Modern </a>
                                                                 </div>
                                                                 <div class="item-price">
                                                                     <div class="price-box">
                                                                <span class="regular-price">
                                                                    <span class="price">
                                                                        <span class="price1">$ 540.00</span>
                                                                    </span>
                                                                </span>
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="action-bot">
                                                             <div class="wrap-addtocart">
                                                                 <button class="btn-cart"
                                                                         title="Add to Cart">
                                                                     <i class="fa fa-shopping-cart"></i>
                                                                     <span>Add to Cart</span>
                                                                 </button>
                                                             </div>
                                                             <div class="actions">
                                                                 <ul class="add-to-links">
                                                                     <li>
                                                                         <a class="link-wishlist"
                                                                            href="#"
                                                                            title="Add to Wishlist">
                                                                             <i class="fa fa-heart"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li>
                                                                         <a class="link-compare"
                                                                            href="#"
                                                                            title="Add to Compare">
                                                                             <i class="fa fa-random"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li class="wrap-quickview" data-id="qv_item_7">
                                                                         <div class="quickview-wrap">
                                                                             <a class="sns-btn-quickview qv_btn"
                                                                                href="#">
                                                                                 <i class="fa fa-eye"></i>
                                                                             </a>
                                                                         </div>
                                                                     </li>
                                                                 </ul>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>

                                             <div class="item col-lg-2d4 col-md-3 col-sm-4 col-xs-6 col-phone-12">
                                                 <div class="item-inner">
                                                     <div class="prd">
                                                         <div class="item-img clearfix">
                                                             <div class="ico-label"></div>
                                                             <a class="product-image have-additional"
                                                                title="Modular Modern"
                                                                href="index3-detail.html">
                                                                <span class="img-main">
                                                               <img src="<?php echo base_url().'assets/images/products/22.jpg';?>" alt="">
                                                                </span>
                                                             </a>
                                                         </div>
                                                         <div class="item-info">
                                                             <div class="info-inner">
                                                                 <div class="item-title">
                                                                     <a title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                         Modular Modern </a>
                                                                 </div>
                                                                 <div class="item-price">
                                                                     <div class="price-box">
                                                                <span class="regular-price">
                                                                    <span class="price">
                                                                        <span class="price1">$ 540.00</span>
                                                                    </span>
                                                                </span>
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="action-bot">
                                                             <div class="wrap-addtocart">
                                                                 <button class="btn-cart"
                                                                         title="Add to Cart">
                                                                     <i class="fa fa-shopping-cart"></i>
                                                                     <span>Add to Cart</span>
                                                                 </button>
                                                             </div>
                                                             <div class="actions">
                                                                 <ul class="add-to-links">
                                                                     <li>
                                                                         <a class="link-wishlist"
                                                                            href="#"
                                                                            title="Add to Wishlist">
                                                                             <i class="fa fa-heart"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li>
                                                                         <a class="link-compare"
                                                                            href="#"
                                                                            title="Add to Compare">
                                                                             <i class="fa fa-random"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li class="wrap-quickview" data-id="qv_item_7">
                                                                         <div class="quickview-wrap">
                                                                             <a class="sns-btn-quickview qv_btn"
                                                                                href="#">
                                                                                 <i class="fa fa-eye"></i>
                                                                             </a>
                                                                         </div>
                                                                     </li>
                                                                 </ul>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>

                                             <div class="item col-lg-2d4 col-md-3 col-sm-4 col-xs-6 col-phone-12">
                                                 <div class="item-inner">
                                                     <div class="prd">
                                                         <div class="item-img clearfix">
                                                             <div class="ico-label"></div>
                                                             <a class="product-image have-additional"
                                                                title="Modular Modern"
                                                                href="index3-detail.html">
                                                                <span class="img-main">
                                                               <img src="<?php echo base_url().'assets/images/products/21.jpg';?>" alt="">
                                                                </span>
                                                             </a>
                                                         </div>
                                                         <div class="item-info">
                                                             <div class="info-inner">
                                                                 <div class="item-title">
                                                                     <a title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                         Modular Modern </a>
                                                                 </div>
                                                                 <div class="item-price">
                                                                     <div class="price-box">
                                                                <span class="regular-price">
                                                                    <span class="price">
                                                                        <span class="price1">$ 540.00</span>
                                                                    </span>
                                                                </span>
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="action-bot">
                                                             <div class="wrap-addtocart">
                                                                 <button class="btn-cart"
                                                                         title="Add to Cart">
                                                                     <i class="fa fa-shopping-cart"></i>
                                                                     <span>Add to Cart</span>
                                                                 </button>
                                                             </div>
                                                             <div class="actions">
                                                                 <ul class="add-to-links">
                                                                     <li>
                                                                         <a class="link-wishlist"
                                                                            href="#"
                                                                            title="Add to Wishlist">
                                                                             <i class="fa fa-heart"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li>
                                                                         <a class="link-compare"
                                                                            href="#"
                                                                            title="Add to Compare">
                                                                             <i class="fa fa-random"></i>
                                                                         </a>
                                                                     </li>
                                                                     <li class="wrap-quickview" data-id="qv_item_7">
                                                                         <div class="quickview-wrap">
                                                                             <a class="sns-btn-quickview qv_btn"
                                                                                href="#">
                                                                                 <i class="fa fa-eye"></i>
                                                                             </a>
                                                                         </div>
                                                                     </li>
                                                                 </ul>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>
                                        </div>
                                    </div>
                                  </div>
                                  <!-- <h3 class="bt-more">
                                    <span>Load more items</span>
                                  </h3> -->
                                </div>   
                               <!-- <div class="sns-products-list">
                                    <div class="row"> 
                                        <div class="products-small" style="display: inline-block">
                                            <div class="item-row col-md-4 col-sm-6 col-lg-3">
                                                <h3>Featured</h3>
                                                <div class="item-content">
                                                    <div class="item">
                                                        <div class="item-inner">
                                                             <div class="prd">
                                                                 <div class="item-img clearfix">
                                                                     <a class="product-image have-additional"
                                                                        title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                        <span class="img-main">
                                                                       <img src="<?php echo base_url().'assets/images/images/1.png';?>" alt="">
                                                                        </span>
                                                                     </a>
                                                                 </div>
                                                                 <div class="item-info">
                                                                     <div class="info-inner">
                                                                         <div class="item-title">
                                                                             <a title="Modular Modern"
                                                                                href="index3-detail.html">
                                                                                 Logo Desingning </a>
                                                                         </div>
                                                                         <div class="item-price">
                                                                             <div class="price-box">
                                                                        <span class="regular-price">
                                                                            <span class="price">
                                                                                <span class="price1">$ 540.00</span>
                                                                                <span class="price2">$ 600.00</span>
                                                                            </span>
                                                                        </span>
                                                                             </div>
                                                                         </div>
                                                                     </div>
                                                                    
                                                                 </div>
                                                             </div>
                                                         </div>
                                                    </div>
                                                    <div class="item">
                                                        <div class="item-inner">
                                                             <div class="prd">
                                                                 <div class="item-img clearfix">
                                                                     <a class="product-image have-additional"
                                                                        title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                        <span class="img-main">
                                                                       <img src="<?php echo base_url().'assets/images/images/3.png';?>" alt="">
                                                                        </span>
                                                                     </a>
                                                                 </div>
                                                                 <div class="item-info">
                                                                     <div class="info-inner">
                                                                         <div class="item-title">
                                                                             <a title="Modular Modern"
                                                                                href="index3-detail.html">
                                                                                 Static Website </a>
                                                                         </div>
                                                                         <div class="item-price">
                                                                             <div class="price-box">
                                                                        <span class="regular-price">
                                                                            <span class="price">
                                                                                <span class="price1">$ 540.00</span>
                                                                            </span>
                                                                        </span>
                                                                             </div>
                                                                         </div>
                                                                     </div>
                                                                     
                                                                 </div>
                                                                 
                                                             </div>
                                                         </div>
                                                    </div>
                                                    <div class="item">
                                                        <div class="item-inner">
                                                             <div class="prd">
                                                                 <div class="item-img clearfix">
                                                                     <a class="product-image have-additional"
                                                                        title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                        <span class="img-main">
                                                                       <img src="<?php echo base_url().'assets/images/images/2.png';?>" alt="">
                                                                        </span>
                                                                     </a>
                                                                 </div>
                                                                 <div class="item-info">
                                                                     <div class="info-inner">
                                                                         <div class="item-title">
                                                                             <a title="Modular Modern"
                                                                                href="index3-detail.html">
                                                                                 Virtual Visiting Card </a>
                                                                         </div>
                                                                         <div class="item-price">
                                                                             <div class="price-box">
                                                                        <span class="regular-price">
                                                                            <span class="price">
                                                                                <span class="price1">$ 540.00</span>
                                                                            </span>
                                                                        </span>
                                                                             </div>
                                                                         </div>
                                                                     </div>
                                                                    
                                                                 </div>
                                                                 
                                                             </div>
                                                         </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="item-row col-md-4 col-sm-6 col-lg-3">
                                                <h3>Best sale</h3>
                                                <div class="item-content">
                                                    <div class="item">
                                                        <div class="item-inner">
                                                             <div class="prd">
                                                                 <div class="item-img clearfix">
                                                                     <a class="product-image have-additional"
                                                                        title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                        <span class="img-main">
                                                                       <img src="<?php echo base_url().'assets/images/images/4.png';?>" alt="">
                                                                        </span>
                                                                     </a>
                                                                 </div>
                                                                 <div class="item-info">
                                                                     <div class="info-inner">
                                                                         <div class="item-title">
                                                                             <a title="Modular Modern"
                                                                                href="index3-detail.html">
                                                                                 Logo Designing</a>
                                                                         </div>
                                                                         <div class="item-price">
                                                                             <div class="price-box">
                                                                        <span class="regular-price">
                                                                            <span class="price">
                                                                                <span class="price1">$ 540.00</span>
                                                                            </span>
                                                                        </span>
                                                                             </div>
                                                                         </div>
                                                                     </div>
                                                                    
                                                                 </div>
                                                                 
                                                             </div>
                                                         </div>
                                                    </div>
                                                    <div class="item">
                                                        <div class="item-inner">
                                                             <div class="prd">
                                                                 <div class="item-img clearfix">
                                                                     <a class="product-image have-additional"
                                                                        title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                        <span class="img-main">
                                                                       <img src="<?php echo base_url().'assets/images/images/5.png';?>" alt="">
                                                                        </span>
                                                                     </a>
                                                                 </div>
                                                                 <div class="item-info">
                                                                     <div class="info-inner">
                                                                         <div class="item-title">
                                                                             <a title="Modular Modern"
                                                                                href="index3-detail.html">
                                                                                 Virtual Visiting Card </a>
                                                                         </div>
                                                                         <div class="item-price">
                                                                             <div class="price-box">
                                                                        <span class="regular-price">
                                                                            <span class="price">
                                                                                <span class="price1">$ 540.00</span>
                                                                            </span>
                                                                        </span>
                                                                             </div>
                                                                         </div>
                                                                     </div>
                                                                    
                                                                 </div>
                                                                 
                                                             </div>
                                                         </div>
                                                    </div>
                                                    <div class="item">
                                                        <div class="item-inner">
                                                             <div class="prd">
                                                                 <div class="item-img clearfix">
                                                                     <a class="product-image have-additional"
                                                                        title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                        <span class="img-main">
                                                                       <img src="<?php echo base_url().'assets/images/images/6.png';?>" alt="">
                                                                        </span>
                                                                     </a>
                                                                 </div>
                                                                 <div class="item-info">
                                                                     <div class="info-inner">
                                                                         <div class="item-title">
                                                                             <a title="Modular Modern"
                                                                                href="index3-detail.html">
                                                                                 Static Website </a>
                                                                         </div>
                                                                         <div class="item-price">
                                                                             <div class="price-box">
                                                                        <span class="regular-price">
                                                                            <span class="price">
                                                                                <span class="price1">$ 540.00</span>
                                                                            </span>
                                                                        </span>
                                                                             </div>
                                                                         </div>
                                                                     </div>
                                                                    
                                                                 </div>
                                                                 
                                                             </div>
                                                         </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="item-row col-md-4 col-sm-6 col-lg-3">
                                                <h3>Most viewed</h3>
                                                <div class="item-content">
                                                    <div class="item">
                                                        <div class="item-inner">
                                                             <div class="prd">
                                                                 <div class="item-img clearfix">
                                                                     <a class="product-image have-additional"
                                                                        title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                        <span class="img-main">
                                                                       <img src="<?php echo base_url().'assets/images/images/1.png';?>" alt="">
                                                                        </span>
                                                                     </a>
                                                                 </div>
                                                                 <div class="item-info">
                                                                     <div class="info-inner">
                                                                         <div class="item-title">
                                                                             <a title="Modular Modern"
                                                                                href="index3-detail.html">
                                                                                 Logo Designing </a>
                                                                         </div>
                                                                         <div class="item-price">
                                                                             <div class="price-box">
                                                                        <span class="regular-price">
                                                                            <span class="price">
                                                                                <span class="price1">$ 540.00</span>
                                                                            </span>
                                                                        </span>
                                                                             </div>
                                                                         </div>
                                                                     </div>
                                                                    
                                                                 </div>
                                                                 
                                                             </div>
                                                         </div>
                                                    </div>
                                                    <div class="item">
                                                        <div class="item-inner">
                                                             <div class="prd">
                                                                 <div class="item-img clearfix">
                                                                     <a class="product-image have-additional"
                                                                        title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                        <span class="img-main">
                                                                       <img src="<?php echo base_url().'assets/images/images/2.png';?>" alt="">
                                                                        </span>
                                                                     </a>
                                                                 </div>
                                                                 <div class="item-info">
                                                                     <div class="info-inner">
                                                                         <div class="item-title">
                                                                             <a title="Modular Modern"
                                                                                href="index3-detail.html">
                                                                                 Virtual Visiting Card </a>
                                                                         </div>
                                                                         <div class="item-price">
                                                                             <div class="price-box">
                                                                        <span class="regular-price">
                                                                            <span class="price">
                                                                                <span class="price1">$ 540.00</span>
                                                                            </span>
                                                                        </span>
                                                                             </div>
                                                                         </div>
                                                                     </div>
                                                                    
                                                                 </div>
                                                                 
                                                             </div>
                                                         </div>
                                                    </div>
                                                    <div class="item">
                                                        <div class="item-inner">
                                                             <div class="prd">
                                                                 <div class="item-img clearfix">
                                                                     <a class="product-image have-additional"
                                                                        title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                        <span class="img-main">
                                                                       <img src="<?php echo base_url().'assets/images/images/3.png';?>" alt="">
                                                                        </span>
                                                                     </a>
                                                                 </div>
                                                                 <div class="item-info">
                                                                     <div class="info-inner">
                                                                         <div class="item-title">
                                                                             <a title="Modular Modern"
                                                                                href="index3-detail.html">
                                                                                 Static Website </a>
                                                                         </div>
                                                                         <div class="item-price">
                                                                             <div class="price-box">
                                                                        <span class="regular-price">
                                                                            <span class="price">
                                                                                <span class="price1">$ 540.00</span>
                                                                            </span>
                                                                        </span>
                                                                             </div>
                                                                         </div>
                                                                     </div>
                                                                    
                                                                 </div>
                                                                 
                                                             </div>
                                                         </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="item-row col-md-4 col-sm-6 col-lg-3">
                                                <h3>Top rate</h3>
                                                <div class="item-content">
                                                    <div class="item">
                                                        <div class="item-inner">
                                                             <div class="prd">
                                                                 <div class="item-img clearfix">
                                                                     <a class="product-image have-additional"
                                                                        title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                        <span class="img-main">
                                                                       <img src="<?php echo base_url().'assets/images/images/4.png';?>" alt="">
                                                                        </span>
                                                                     </a>
                                                                 </div>
                                                                 <div class="item-info">
                                                                     <div class="info-inner">
                                                                         <div class="item-title">
                                                                             <a title="Modular Modern"
                                                                                href="index3-detail.html">
                                                                                 Logo Designing </a>
                                                                         </div>
                                                                         <div class="item-price">
                                                                             <div class="price-box">
                                                                        <span class="regular-price">
                                                                            <span class="price">
                                                                                <span class="price1">$ 540.00</span>
                                                                            </span>
                                                                        </span>
                                                                             </div>
                                                                         </div>
                                                                     </div>
                                                                    
                                                                 </div>
                                                                 
                                                             </div>
                                                         </div>
                                                    </div>

                                                     <div class="item">
                                                        <div class="item-inner">
                                                             <div class="prd">
                                                                 <div class="item-img clearfix">
                                                                     <a class="product-image have-additional"
                                                                        title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                        <span class="img-main">
                                                                       <img src="<?php echo base_url().'assets/images/images/5.png';?>" alt="">
                                                                        </span>
                                                                     </a>
                                                                 </div>
                                                                 <div class="item-info">
                                                                     <div class="info-inner">
                                                                         <div class="item-title">
                                                                             <a title="Modular Modern"
                                                                                href="index3-detail.html">
                                                                                 Virtual Visiting Card </a>
                                                                         </div>
                                                                         <div class="item-price">
                                                                             <div class="price-box">
                                                                        <span class="regular-price">
                                                                            <span class="price">
                                                                                <span class="price1">$ 540.00</span>
                                                                            </span>
                                                                        </span>
                                                                             </div>
                                                                         </div>
                                                                     </div>
                                                                    

                                                                 </div>
                                                                 
                                                             </div>
                                                         </div>
                                                    </div>
                                                    <div class="item">
                                                        <div class="item-inner">
                                                             <div class="prd">
                                                                 <div class="item-img clearfix">
                                                                     <a class="product-image have-additional"
                                                                        title="Modular Modern"
                                                                        href="index3-detail.html">
                                                                        <span class="img-main">
                                                                       <img src="<?php echo base_url().'assets/images/images/6.png';?>" alt="">
                                                                        </span>
                                                                     </a>
                                                                 </div>
                                                                 <div class="item-info">
                                                                     <div class="info-inner">
                                                                         <div class="item-title">
                                                                             <a title="Modular Modern"
                                                                                href="index3-detail.html">
                                                                                Static Website </a>
                                                                         </div>
                                                                         <div class="item-price">
                                                                             <div class="price-box">
                                                                        <span class="regular-price">
                                                                            <span class="price">
                                                                                <span class="price1">$ 540.00</span>
                                                                            </span>
                                                                        </span>
                                                                             </div>
                                                                         </div>
                                                                     </div>
                                                                    
                                                                 </div>
                                                                 
                                                             </div>
                                                         </div>
                                                    </div>
                                                </div>
                                            </div>

                                           
                                        </div>
                                    </div>
                                </div> -->

                                 <!-- <div id="header-slideshow">
                                    <div class="row">
                                        <div class="slideshows col-md-6 col-sm-8">
                                            <div id="slider123456">
                                                <div class="item style1 banner5">
                                                    <a href="#">
                                                        <img src="<?php echo base_url().'assets/images/sildeshow/slideshow1.jpg';?>" alt="">
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="banner-right col-md-6 col-sm-4">
                                             <div class="banner6 banner5 dbn col-md-12 col-sm-6">
                                                <a href="#">
                                                    <img src="<?php echo base_url().'assets/images/sildeshow/banner1.jpg';?>" alt="">
                                                </a>
                                            </div>
                                             <div class="banner6 pdno col-md-12 col-sm-12">
                                                <div class="banner7 banner6  banner5 col-md-6 col-sm-12">
                                                    <a href="#">
                                                        <img src="<?php echo base_url().'assets/images/sildeshow/banner2.jpg';?>" alt="">
                                                    </a>
                                                </div>
                                                <div class="banner8 banner6  banner5 col-md-6 col-sm-12">
                                                    <a href="#">
                                                        <img src="<?php echo base_url().'assets/images/sildeshow/banner3.jpg';?>" alt="">
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> -->
                             </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- AND CONTENT -->
