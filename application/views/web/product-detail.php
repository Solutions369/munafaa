

            <!-- BREADCRUMBS -->
            <div id="sns_breadcrumbs" class="wrap">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="sns_titlepage"></div>
                            <div id="sns_pathway" class="clearfix">
                                <div class="pathway-inner">
                                    <span class="icon-pointer "></span>
                                    <ul class="breadcrumbs">
                                        <li class="home">
                                            <a title="Go to Home Page" href="<?php echo site_url("index"); ?>">
                                                <i class="fa fa-home"></i>
                                                <span>Home</span>
                                            </a>
                                        </li>
                                        <li class="category3 last">
                                            <span><?php if(!empty($service[0]->name)){ echo $service[0]->name; } ?></span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- AND BREADCRUMBS -->

            <!-- CONTENT -->
            <div id="sns_content" class="wrap layout-m">
                <div class="container">
                    <div class="row">
                        <div id="sns_main" class="col-md-12 col-main">
                            <div id="sns_mainmidle">
                                <div class="product-view sns-product-detail">
                                    <div class="product-essential clearfix">
                                        <div class="row row-img">

                                            <div class="product-img-box col-md-4 col-sm-5">
                                                <div class="detail-img">
                                                <?php if($service[0]->image){ ?>
                                                    <img src="<?php echo base_url().'uploads/products/'.$service[0]->image;?>" alt="">
                                                <?php }else{ ?>
                                                    <img src="<?php echo base_url().'assets/images/default.png';?>" alt="">
                                                <?php } ?>
                                                </div>
                                            </div>
                                            <div id="product_shop" class="product-shop col-md-8 col-sm-7">
                                                <div class="item-inner product_list_style">
                                                    <div class="item-info">
                                                        <div class="item-title">
                                                            <a title="<?php if(!empty($service[0]->name)){ echo $service[0]->name; } ?>" href="index3-detail.html"><?php if(!empty($service[0]->name)){ echo $service[0]->name; } ?></a>
                                                        </div>
                                                        <div class="item-price">
                                                            <div class="price-box">
                                                                <span class="regular-price">
                                                                    <span class="price"><?php if(!empty($service[0]->price)){ echo $service[0]->price.'/-'; } ?></span>
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="availability">
                                                            <p class="style1">Availability: In stock</p>
                                                        </div>
                                                        <div class="rating-block">
                                                            <div class="ratings">
                                                                <div class="rating-box">
                                                                    <div class="rating" style="width:60%"></div>
                                                                </div>
                                                               <span class="amount">
                                                                    <a href="#">(1 Reviews)</a>
                                                                    <span class="separator">|</span>
                                                                    <a href="#">Add Your Review</a>
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="desc std">
                                                            <h5>QUICK OVERVIEW</h5>
                                                            <p><?php if(!empty($service[0]->quick_overview)){ echo $service[0]->quick_overview; } ?></p>
                                                        </div>

                                                        <div class="actions">                                         
                                                            <button type="button" class="btn-cart addToCart" value="<?php echo $service[0]->service_id; ?>">
                                                                Add to Cart
                                                            </button>
                                                            <ul class="add-to-links">
                                                                <li>
                                                                    <a class="link-wishlist" data-original-title="Add to Wishlist" data-toggle="tooltip" href="#" title=""></a>
                                                                </li>
                                                                <li>
                                                                    <a class="link-compare" data-original-title="Add to Compare" data-toggle="tooltip" href="#" title=""></a>
                                                                </li>
                                                                <li>
                                                                    <div class="wrap-quickview" data-id="qv_item_8">
                                                                        <div class="quickview-wrap">
                                                                            <a class="sns-btn-quickview qv_btn" data-original-title="View" data-toggle="tooltip" href="#">
                                                                                <span>View</span>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                            <div class="addthis_native_toolbox"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="bottom row">
                        <div class="2coloum-left">
                            <div id="sns_mainm" class="col-md-12">
                                <div id="sns_description" class="description">
                                    <div class="sns_producttaps_wraps1">
                                        <h3 class="detail-none">Description
                                            <i class="fa fa-align-justify"></i>
                                        </h3>
                                          <!-- Nav tabs -->
                                        <ul class="nav nav-tabs" role="tablist">
                                            <li role="presentation" class="active style-detail"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Product Description</a></li>
                                            <li role="presentation" class="style-detail"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Reviews</a></li>
                                            <!-- <li role="presentation" class="style-detail"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Product Tags</a></li> -->
                                        </ul>
                                          <!-- Tab panes -->
                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane active" id="home">
                                                <div class="style1">
                                                    <p class="top">
                                                    <?php if(!empty($service[0]->description)){ echo $service[0]->description; } ?>
                                                    </p>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="profile">
                                                <div class="collateral-box">
                                                    <div class="form-add">
                                                        <h2>Write Your Own Review</h2>
                                                        <form id="review-form">
                                                            <input type="hidden" value="8haZqMXtybxMqfBa" name="form_key">
                                                            <fieldset>
                                                                <h3>
                                                                    You're reviewing:
                                                                    <span>Cfg Armani Blue</span>
                                                                </h3>
                                                                <ul class="form-list">
                                                                    <li>
                                                                        <label class="required" for="nickname_field">
                                                                            <em>*</em>
                                                                            Nickname
                                                                        </label>
                                                                        <div class="input-box">
                                                                            <input id="nickname_field" class="input-text required-entry" type="text" value="" name="nickname">
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <label class="required" for="summary_field">
                                                                            <em>*</em>
                                                                            Summary of Your Review
                                                                        </label>
                                                                        <div class="input-box">
                                                                            <input id="summary_field" class="input-text required-entry" type="text" value="" name="title">
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <label class="required" for="review_field">
                                                                            <em>*</em>
                                                                            Review
                                                                        </label>
                                                                        <div class="input-box">
                                                                            <textarea id="review_field" class="required-entry" rows="3" cols="5" name="detail"></textarea>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </fieldset>
                                                            <div class="buttons-set">
                                                                <button class="button" title="Submit Review" type="submit">
                                                                    <span>
                                                                        <span>Submit Review</span>
                                                                    </span>
                                                                </button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="messages">
                                                <div class="collateral-box">
                                                    <p>
                                                        <img alt="" src="http://placehold.it/240x180" style="margin-top: 5px;">
                                                    </p>
                                                    <p>Retra faucibus eu laoreet nunc. Tincidunt nulla a Nulla eu convallis scelerisque sociis nulla interdum et. Cursus senectus aliquet pretium at tristique hac ullamcorper adipiscing et Donec. Enim montes parturient.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>



                                <div class="products-upsell">
                                    <div class="detai-products1">
                                        <div class="title">
                                            <h3>Related Services</h3>
                                        </div>
                                        <div class="products-grid">
                                            <div id="related_upsell" class="item-row owl-carousel owl-theme" style="display: inline-block">
                                            <?php foreach($related as $row){ ?>
                                                <div class="item">
                                                    <div class="item-inner">
                                                        <div class="prd">
                                                            <div class="item-img clearfix">
                                                                
                                                                <a class="product-image have-additional" href="<?php echo site_url("ProductDetail/".$row->service_id); ?>" title="Modular Modern">
                                                                    <span class="img-main">
                                                                    <?php if(!empty($row->image)){ ?>
                                                                        <img alt="" src="<?php echo base_url().'uploads/products/'.$row->image; ?>">
                                                                    <?php }else{ ?>
                                                                        <img src="<?php echo base_url().'assets/images/default.png';?>" alt="">
                                                                    <?php } ?>
                                                                    </span>
                                                                </a>
                                                            </div>
                                                            <div class="item-info">
                                                                <div class="info-inner">
                                                                    <div class="item-title">
                                                                        <a href="index3-detail.html" title="<?php echo $row->name; ?>"> <?php echo $row->name; ?> </a>
                                                                    </div>
                                                                    <div class="item-price">
                                                                        <div class="price-box">
                                                                            <span class="regular-price">
                                                                                <span class="price">
                                                                                    <span class="price1"><?php echo $row->price.'/-'; ?></span>
                                                                                    <!-- <span class="price2">$ 600.00</span> -->
                                                                                </span>
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="action-bot">
                                                                <div class="wrap-addtocart">
                                                                    <button class="btn-cart addToCart" value="<?php echo $row->service_id; ?>" title="Add to Cart">
                                                                        <i class="fa fa-shopping-cart"></i>
                                                                        <span>Add to Cart</span>
                                                                    </button>
                                                                </div>
                                                                <div class="actions">
                                                                    <ul class="add-to-links">
                                                                        <li>
                                                                            <a class="link-wishlist" title="Add to Wishlist" href="#">
                                                                                <i class="fa fa-heart"></i>
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a class="link-compare" title="Add to Compare" href="#">
                                                                                <i class="fa fa-random"></i>
                                                                            </a>
                                                                        </li>
                                                                        <li class="wrap-quickview" data-id="qv_item_7">
                                                                            <div class="quickview-wrap">
                                                                                <a class="sns-btn-quickview qv_btn" href="#">
                                                                                    <i class="fa fa-eye"></i>
                                                                                </a>
                                                                            </div>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- AND CONTENT -->

            <!-- PARTNERS -->
            <div id="sns_partners" class="wrap">
                <div class="container">
                    <div class="slider-wrap">
                        <div class="partners_slider_in">
                            <div id="partners_slider1" class="our_partners owl-carousel owl-theme owl-loaded" style="display: inline-block">
                                <div class="item">
                                    <a class="banner11" href="#" target="_blank">
                                        <img alt="" src="images/brands/1.png">
                                    </a>
                                </div>
                                <div class="item">
                                    <a class="banner11" href="#" target="_blank">
                                        <img alt="" src="images/brands/2.png">
                                    </a>
                                </div>
                                <div class="item">
                                    <a class="banner11" href="#" target="_blank">
                                        <img alt="" src="images/brands/3.png">
                                    </a>
                                </div>
                                <div class="item">
                                    <a class="banner11" href="#" target="_blank">
                                        <img alt="" src="images/brands/4.png">
                                    </a>
                                </div>
                                <div class="item">
                                    <a class="banner11" href="#" target="_blank">
                                        <img alt="" src="images/brands/5.png">
                                    </a>
                                </div>
                                <div class="item">
                                    <a class="banner11" href="#" target="_blank">
                                        <img alt="" src="images/brands/6.png">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- AND PARTNERS -->
